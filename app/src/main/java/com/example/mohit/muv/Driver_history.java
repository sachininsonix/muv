package com.example.mohit.muv;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.mohit.muv.LoginRegister.AppController;
import com.example.mohit.muv.Navigation_Drawer.First_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Second_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Third_Fragment;
import com.example.mohit.muv.constant.Iconstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

public class Driver_history extends AppCompatActivity implements Iconstant {
ListView driverlist;
    DrawerLayout mDrawer;
    NavigationView nvDrawer;
    Toolbar toolbar;

    ArrayList<HashMap<String,String >> addme;
    ProgressDialog progressDialog;
    String url;
    CategoryAdapter categoryAdapter;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ImageLoader imageLoader;
    DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_history);
        driverlist=(ListView)findViewById(R.id.list);
        progressDialog=new ProgressDialog(Driver_history.this);
        addme=new ArrayList<>();
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        preferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        imageLoader= ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(Driver_history.this));
        options = new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.blank).cacheOnDisc().cacheInMemory().build();
//        Driver_history_adapter driver_history_adapter=new Driver_history_adapter(Driver_history.this);
//        listView.setAdapter(driver_history_adapter);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        //  navigation_btn=(ImageView)findViewById(R.id.navigation_btn);
        setupDrawerContent(nvDrawer);
        toolbar.setNavigationIcon(R.drawable.navigation);
        toolbar.setTitle("MUV");
        toolbar.setTitleTextColor(Color.GRAY);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });
//        driverlist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view,
//                                           int position, long id) {
//                // TODO Auto-generated method stub
//
//                addme.remove(position);
//
//                CategoryAdapter
//
//                Toast.makeText(Driver_history.this, "Item Deleted", Toast.LENGTH_LONG).show();
//
//                return true;
//            }
//
//        });
//        driverlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//        @Override
//        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            Intent intent = new Intent(Driver_history.this, Booking_detail.class);
//            intent.putExtra("hashme", addme);
//            intent.putExtra("position",position);
//            startActivity(intent);
//
//        }
//    });

    driverhistory();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_driver_history, menu);
        return true;
    }
    public void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override

                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }

                });
    }
    public void selectDrawerItem(MenuItem menuItem) {
//        Fragment fragment = null;
        // Class fragmentClass;
        switch(menuItem.getItemId()) {
            case R.id.nav_first_fragment:
                Intent intent=new Intent(Driver_history.this,First_Fragment.class);

                startActivity(intent);
//                finish();
                //finish();
                // fragmentClass = FirstFragment.class;
                break;
            case R.id.nav_second_fragment:
                Intent intent1=new Intent(Driver_history.this,Second_Fragment.class);
                startActivity(intent1);
//                finish();
                //  fragmentClass = Second_Fragment.class;
                // finish();
                break;
//            case R.id.nav_third_fragment:
//
//                Intent intent3=new Intent(Driver_history.this,Third_Fragment.class);
//                startActivity(intent3);
//
//                break;

            default:
                Intent intent11=new Intent(Driver_history.this,First_Fragment.class);
                startActivity(intent11);
//                finish();
                //finish();
                // fragmentClass = FirstFragment.class;
        }
        setTitle(menuItem.getTitle());
        mDrawer.closeDrawers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void driverhistory(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = "http://insonix.com/design.insonix.com/muv/get_bookins_driver.php?driver_id="+preferences.getString(uUid,"") +"&type="+1;
        Log.d("Url", "url" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    if(status.equalsIgnoreCase("ok")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("bookings");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject catlist = jsonArray.getJSONObject(i);
                            String booking_id = catlist.getString("booking_id");
                            String driver_id = catlist.getString("driver_id");
                            String user_id = catlist.getString("user_id");
                            String user_name = catlist.getString("user_name");
                            String distance = catlist.getString("distance");
                            String fair = catlist.getString("fair");
                            String loc_name = catlist.getString("loc_name");
                            String droplocation = catlist.getString("droplocation");
                            String no_of_pas = catlist.getString("no_of_pas");
                            String pick_up_lat = catlist.getString("pick_up_lat");
                            String pick_up_lon = catlist.getString("pick_up_lon");
                            String drop_off_lat = catlist.getString("drop_off_lat");
                            String drop_off_lon = catlist.getString("drop_off_lon");
                            String time = catlist.getString("time");
                            String phone = catlist.getString("phone");
                            String rating = catlist.getString("rating");

                            String profile_file = catlist.getString("profile_file");


                            Log.d("userName", "userName" + user_name);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("booking_id", booking_id);
                            hashMap.put("driver_id", driver_id);
                            hashMap.put("user_id", user_id);
                            hashMap.put("user_name", user_name);
                            hashMap.put("distance", distance);
                            hashMap.put("fair", fair);
                            hashMap.put("loc_name", loc_name);
                            hashMap.put("droplocation", droplocation);
                            hashMap.put("no_of_pas", no_of_pas);
                            hashMap.put("pick_up_lat", pick_up_lat);
                            hashMap.put("pick_up_lon", pick_up_lon);
                            hashMap.put("drop_off_lat", drop_off_lat);
                            hashMap.put("drop_off_lon", drop_off_lon);
                            hashMap.put("drop_off_lon", drop_off_lon);
                            hashMap.put("time", time);
                            hashMap.put("phone", phone);
                            hashMap.put("profile_file", profile_file);
                            hashMap.put("rating", rating);
                            addme.add(hashMap);
                        }
                        // addme = new ArrayList<>(addme);
//                        categoryAdapter = new CategoryAdapter(getActivity(), addme);
//                        taboneList.setAdapter(categoryAdapter);
                        driverlist.setAdapter(new CategoryAdapter(Driver_history.this, addme));
                        //no_found.setVisibility(View.GONE);
                    }else{
                        new ShowMsg().createDialog(Driver_history.this, "No Booking Available!");
                        // no_found.setVisibility(View.VISIBLE);
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class CategoryAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> addme;
        public CategoryAdapter(Activity activity,ArrayList<HashMap<String,String>>addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.vehicle_list,null);
            ImageView cat_image=(ImageView)convertView.findViewById(R.id.sell_pic);
            TextView drivrName=(TextView)convertView.findViewById(R.id.drivrName);
            RatingBar ratingVal=(RatingBar)convertView.findViewById(R.id.rtbProductRating);
            //RelativeLayout rel_help=(RelativeLayout)convertView.findViewById(R.id.rel_help);
            drivrName.setText(addme.get(position).get("user_name"));
            Log.d("Rating","Rating"+addme.get(position).get("rating"));
            try {
            if(addme.get(position).get("rating").equals("") ){
            }else{
                ratingVal.setRating(Float.parseFloat(addme.get(position).get("rating")));
            }}
            catch (Exception e){

            }
            if(addme.get(position).get("profile_file").equals("") ){
            }else{
                imageLoader.displayImage(addme.get(position).get("profile_file"), cat_image, options);

            }
                        convertView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent(Driver_history.this, Booking_detail.class);
                                intent.putExtra("hashme", addme);
                                intent.putExtra("position", position);
                                startActivity(intent);
                            }
                        });
            driverlist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                               int pos, long id) {
                    // TODO Auto-generated method stub

                    Log.v("long clicked", "pos: " + pos);
//                    driverlist.remove(position);
                    driverlist.removeViewAt(pos);
                    return true;
                }
            });


            return convertView;
        }
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}
