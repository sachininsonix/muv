package com.example.mohit.muv.LoginRegister;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mohit.muv.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by mohit on 22-Feb-16.
 */
public class Driver_registeration extends Activity implements AdapterView.OnItemSelectedListener {
    Button btnContinue;
    Spinner d_type,d_year,d_make,d_model;
    LinearLayout d_type_img,d_year_img,d_make_img,d_model_img;
    EditText etVehicleNum;
    Spinner s1,s2,s3;
    String[] mTestArray;
    String[] modelArray;
    String isSpinnerTouched;

    private boolean isDefaultSelection;
    private Spinner spinner1, spinner2,spinner3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_registeration);

//        Intent j = getIntent();
//
//        Toast.makeText(Driver_registeration.this, j.getStringExtra("is_driver"),Toast.LENGTH_LONG).show();

//        Spinner spin1 = (Spinner)findViewById(R.id.spin1);
//        Spinner spin2 = (Spinner)findViewById(R.id.spin2);
//        Spinner spin2 = (Spinner)findViewById(R.id.spin2);
//        Spinner spin2 = (Spinner)findViewById(R.id.spin2);

        final Spinner d_type = (Spinner)findViewById(R.id.d_type);
        final Spinner d_year = (Spinner)findViewById(R.id.d_year);
        final Spinner d_make = (Spinner)findViewById(R.id.d_make);
        final Spinner d_model = (Spinner)findViewById(R.id.d_model);
        btnContinue = (Button)findViewById(R.id.btnContinue);

        d_type_img = (LinearLayout)findViewById(R.id.d_type_img);
        d_year_img = (LinearLayout)findViewById(R.id.d_year_img);
        d_make_img = (LinearLayout)findViewById(R.id.d_make_img);
        d_model_img = (LinearLayout)findViewById(R.id.d_model_img);
        etVehicleNum = (EditText)findViewById(R.id.etVehicleNum);
        mTestArray = getResources().getStringArray(R.array.acc_type);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, mTestArray);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dataAdapter.notifyDataSetChanged();
        d_type.setAdapter(dataAdapter);
//
//        s1 = (Spinner)findViewById(R.id.spinner1);
//        s2 = (Spinner)findViewById(R.id.spinner2);
//        s3 = (Spinner)findViewById(R.id.spinner3);
        addListenerOnSpinnerItemSelection();
        // Set true at onCreate
        isDefaultSelection = true;
        d_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (isDefaultSelection) { //If spinner initializes
                    d_make.setAdapter(null);
//                    spinner.setSelection("Set_here_id_of_data_item_from_storage_which_was_previously_stored");
                    isDefaultSelection = false;
                } else { //If user manually select item
                    d_make.setAdapter(null);
                    addItemsOnSpinner2();
                    d_model.setAdapter(null);

                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
                //User selected same item. Nothing to do.
            }
        });
        d_make.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (isDefaultSelection) { //If spinner initializes
                    d_model.setAdapter(null);
//                    spinner.setSelection("Set_here_id_of_data_item_from_storage_which_was_previously_stored");
                    isDefaultSelection = false;
                } else { //If user manually select item
                    d_model.setAdapter(null);
                    addItemsOnSpinner3();
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
                //User selected same item. Nothing to do.
            }
        });

//        String[] type_items = new String[]{"Select Type","Pickup Truck", "Vans", "Box truck"};
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1, type_items);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        d_type.setAdapter(adapter);


        //String[] year_items = new String[]{"Select Year","Item 1", "Item 2", "Item 3"};
        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        int lastyears=thisYear-16;
        years.add("Select Year");
        for (int i = lastyears; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> adapter_year = new ArrayAdapter<String>(this,R.layout.spinner_item, years);
        adapter_year.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        d_year.setAdapter(adapter_year);
//        String[] picktruck = new String[]{"Select Make","Ford", "Chevrolet", "Dodge","GMC","Toyota","Nissan","Suzuki","Honda"};
//
//        String[] make_items = new String[]{"Select Make"};
//        ArrayAdapter<String> adapter_make = new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1, make_items);
//        adapter_make.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        d_make.setAdapter(adapter_make);
////
////
//        String[] model_items = new String[]{"Select Model"};
//        ArrayAdapter<String> adapter_model = new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1, model_items);
//        adapter_model.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        d_model.setAdapter(adapter_model);

        d_type_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d_type.performClick();
//                Toast.makeText(Driver_registeration.this,"Click",Toast.LENGTH_SHORT).show();
            }
        });

        d_year_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d_year.performClick();
            }
        });

        d_make_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d_make.performClick();
            }
        });

        d_model_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d_model.performClick();
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (d_type.getSelectedItem().equals("Select Type")) {
                    Toast.makeText(Driver_registeration.this,"Type Empty",Toast.LENGTH_LONG).show();
                }
                else if(d_year.getSelectedItem().equals("Select Year")){
                    Toast.makeText(Driver_registeration.this,"Year Empty",Toast.LENGTH_LONG).show();
                }
                else if(d_make.getSelectedItem().equals("Select Make")){
                    Toast.makeText(Driver_registeration.this,"Make Empty",Toast.LENGTH_LONG).show();
                }
                else if(d_model.getSelectedItem().equals("Select Model")){
                    Toast.makeText(Driver_registeration.this,"Model Empty",Toast.LENGTH_LONG).show();
                }
                else if(etVehicleNum.getText().toString().length() == 0 ){
                    etVehicleNum.setError("Vehicle number is required!");
                }
                else{
                    Intent dr = new Intent(Driver_registeration.this,Register.class);
                    dr.putExtra("d_type", d_type.getSelectedItem().toString());
                    dr.putExtra("d_year", d_year.getSelectedItem().toString());
                    dr.putExtra("d_make", d_make.getSelectedItem().toString());
                    dr.putExtra("d_model",d_model.getSelectedItem().toString());
                    dr.putExtra("d_vehicleNumber",etVehicleNum.getText().toString().trim());
                    dr.putExtra("driver_info",1);
                    Log.d("dtype","dtype"+d_type.getSelectedItem().toString()+" d_year"+d_year.getSelectedItem().toString()
                            +" d_make"+d_make.getSelectedItem().toString()+" d_model"+d_model.getSelectedItem().toString()+
                            " d_vehicleNumber"+etVehicleNum.getText().toString().trim());
                    startActivity(dr);
                }


            }

        });


    }
    public void addListenerOnSpinnerItemSelection() {
        d_type = (Spinner) findViewById(R.id.d_type);
//        spinner1.OnItemSelectedListener();
//        addItemsOnSpinner2();
//        addItemsOnSpinner3();
//        spinner1.setOnItemSelectedListener(new MyOnItemSelectedListener());
        d_make = (Spinner) findViewById(R.id.d_make);
//        spinner2.setOnItemSelectedListener(new MyOnItemSelectedListener());
    }
    // add items into spinner dynamically


    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                               long arg3) {
        String text = String.valueOf(d_type.getSelectedItem());
//        addItemsOnSpinner2();
//        addItemsOnSpinner3();
//        spinner1 = (Spinner) findViewById(R.id.spinner1);
//        String sp1= String.valueOf(spinner1 .getSelectedItem());
//        Toast.makeText(Spiiner_example.this, "Selected Country spinner : " + text, Toast.LENGTH_SHORT).show();

    }
    @Override
    public void onNothingSelected(AdapterView parent) {

    }
    public void addItemsOnSpinner2() {


        //  Spinner spinner = (Spinner)findViewById(R.id.spinner1);
        String sp1 = String.valueOf(d_type.getSelectedItem());


//        spinner1 = (Spinner) findViewById(R.id.spinner1);
//        String sp1= String.valueOf(spinner1 .getSelectedItem());
//        Toast.makeText(Spiiner_example.this, "Selected Country spinner : " + sp1, Toast.LENGTH_SHORT).show();

        if(sp1.contentEquals("Pickup Truck")) {
            mTestArray = getResources().getStringArray(R.array.pickup_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dataAdapter.notifyDataSetChanged();
            d_make.setAdapter(dataAdapter);
        }
        if(sp1.contentEquals("Vans")) {
            mTestArray = getResources().getStringArray(R.array.vans);
            ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dataAdapter2.notifyDataSetChanged();
            d_make.setAdapter(dataAdapter2);
        }
        if(sp1.contentEquals("Box Truck")) {
            mTestArray = getResources().getStringArray(R.array.box_truck);
            ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dataAdapter2.notifyDataSetChanged();
            d_make.setAdapter(dataAdapter2);
        }
//        mTestArray = getResources().getStringArray(R.array.pickup_truck);
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_spinner_item, mTestArray);
//        //ArrayAdapter dataAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, list);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner2.setAdapter(dataAdapter);
    }
    public void addItemsOnSpinner3() {
        d_model = (Spinner) findViewById(R.id.d_model);
        String sp2 = String.valueOf(d_make.getSelectedItem());
        String sp1 = String.valueOf(d_type.getSelectedItem());
        if((sp1.contentEquals("Pickup Truck")) && (sp2.contentEquals("Ford")) ){
            modelArray = getResources().getStringArray(R.array.ford_pickup_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, modelArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Vans")) && (sp2.contentEquals("Ford")) ){
            mTestArray = getResources().getStringArray(R.array.ford_vans);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Box Truck")) && (sp2.contentEquals("Ford")) ){
            mTestArray = getResources().getStringArray(R.array.ford_box_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Pickup Truck")) && (sp2.contentEquals("Chevrolet")) ){
            modelArray = getResources().getStringArray(R.array.chevrolet_pickup_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, modelArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Vans")) && (sp2.contentEquals("Chevrolet")) ){
            mTestArray = getResources().getStringArray(R.array.chevrolet_vans);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Box Truck")) && (sp2.contentEquals("Chevrolet")) ){
            mTestArray = getResources().getStringArray(R.array.chevrolet_box_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Pickup Truck")) && (sp2.contentEquals("Dodge")) ){
            modelArray = getResources().getStringArray(R.array.dodge_pickup_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, modelArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Vans")) && (sp2.contentEquals("Dodge")) ){
            mTestArray = getResources().getStringArray(R.array.dodge_vans);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Box Truck")) && (sp2.contentEquals("Dodge")) ){
            mTestArray = getResources().getStringArray(R.array.dodge_box_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Pickup Truck")) && (sp2.contentEquals("GMC")) ){
            modelArray = getResources().getStringArray(R.array.GMC_pickup_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, modelArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Vans")) && (sp2.contentEquals("GMC")) ){
            mTestArray = getResources().getStringArray(R.array.GMC_vans);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Box Truck")) && (sp2.contentEquals("GMC")) ){
            mTestArray = getResources().getStringArray(R.array.GMC_box_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Pickup Truck")) && (sp2.contentEquals("Toyota")) ){
            modelArray = getResources().getStringArray(R.array.Toyota_pickup_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, modelArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Pickup Truck")) && (sp2.contentEquals("Nissan")) ){
            mTestArray = getResources().getStringArray(R.array.Nissan_pickup_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Vans")) && (sp2.contentEquals("Nissan")) ){
            mTestArray = getResources().getStringArray(R.array.Nissan_vans);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Pickup Truck")) && (sp2.contentEquals("Suzuki")) ){
            modelArray = getResources().getStringArray(R.array.Suzuki_pickup_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, modelArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Pickup Truck")) && (sp2.contentEquals("Honda")) ){
            mTestArray = getResources().getStringArray(R.array.Honda_pickup_truck);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Vans")) && (sp2.contentEquals("Mercedes-Benz")) ){
            mTestArray = getResources().getStringArray(R.array.MercedesBenz_vans);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }
        if((sp1.contentEquals("Vans")) && (sp2.contentEquals("Freightliner")) ){
            mTestArray = getResources().getStringArray(R.array.Freightliner_vans);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item, mTestArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            d_model.setAdapter(dataAdapter);
        }


    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
