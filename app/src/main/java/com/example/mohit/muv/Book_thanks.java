package com.example.mohit.muv;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mohit.muv.Navigation_Drawer.First_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Second_Fragment;

/**
 * Created by insonix on 31/3/16.
 */
public class Book_thanks extends AppCompatActivity {
    Button homescre;
    DrawerLayout mDrawer;
    NavigationView nvDrawer;
    Toolbar toolbar;
    TextView waittext;
    String bookbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thanks);
//        homescre=(Button)findViewById(R.id.homescre);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        waittext=(TextView)findViewById(R.id.textwait);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);

        android.widget.ProgressBar progressindeterminate = new  android.widget.ProgressBar(Book_thanks.this);
        progressindeterminate.setIndeterminate(true);
        //  navigation_btn=(ImageView)findViewById(R.id.navigation_btn);
        setupDrawerContent(nvDrawer);
        toolbar.setNavigationIcon(R.drawable.navigation);
        toolbar.setTitle("MUV");
        toolbar.setTitleTextColor(Color.GRAY);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });
        Bundle extras = getIntent().getExtras();
        try {
            bookbtn = extras.getString("bookbtn");
            Log.d("bookbtn", "bookbtn" + bookbtn);
            if (bookbtn.equals("book_now")) {
                waittext.setText("Thanks for booking! We will update you later.");
            } else {
                waittext.setText("Please be patient! Your Driver will contact you Shortly.");
            }
        }catch(Exception e){}
//        homescre.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(Book_thanks.this,Twoautocompletetetbox.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(0,0);
//            }
//        });
    }
    public void setupDrawerContent(NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(

                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override

                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        selectDrawerItem(menuItem);

                        return true;

                    }

                });

    }
    public void selectDrawerItem(MenuItem menuItem) {

        //Fragment fragment = null;
        //Class fragmentClass;

        switch(menuItem.getItemId()) {

            case R.id.nav_first_fragment:
                Intent intent=new Intent(Book_thanks.this,First_Fragment.class);
                startActivity(intent);
                // fragmentClass = FirstFragment.class;

                break;

            case R.id.nav_second_fragment:
                Intent intent1=new Intent(Book_thanks.this,Second_Fragment.class);
                startActivity(intent1);
                //  fragmentClass = Second_Fragment.class;

                break;

            case R.id.nav_third_fragment:

                // fragmentClass = Third_Fragment.class;

                break;

            default:
                Intent intent11=new Intent(Book_thanks.this,First_Fragment.class);
                startActivity(intent11);
                //  fragmentClass = Third_Fragment.class;

        }






        setTitle(menuItem.getTitle());

        mDrawer.closeDrawers();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(Book_thanks.this,Twoautocompletetetbox.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();

    }
}
