package com.example.mohit.muv;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mohit.muv.LoginRegister.AppController;
import com.example.mohit.muv.Navigation_Drawer.First_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Navigation_drawer;
import com.example.mohit.muv.Navigation_Drawer.Second_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Third_Fragment;
import com.example.mohit.muv.constant.Iconstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mohit on 26-Feb-16.
 */
public class Driver_detail extends Activity implements Iconstant {
    private static final String REGISTER_URL = "http://insonix.com/design.insonix.com/muv/booking.php";

    Button book_now;
    DrawerLayout mDrawer;
    NavigationView nvDrawer;
    Toolbar toolbar;
    ArrayList<HashMap<String,String >> hashme;
    int position,dollar,passengerval,bookLater;
    TextView phonenum,drivername,vehicleyear,vehiclemodel,vehiclenumber;
    SharedPreferences sharedpreferences;
    String pickaddress="",dropaddress,selectedImage,file,distanc,truckTypeimg;
    double lat_pick,lon_pick,lat_drop,lon_drop,dist;
    ProgressDialog progressDialog;
    String url,driverId,bookbtn_value;
    Bitmap bitmap,bmp;
    RatingBar ratingVal;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    ImageView vehicleImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_detail);
        book_now=(Button)findViewById(R.id.book_now);
        phonenum=(TextView)findViewById(R.id.tvPhnNum);
        drivername=(TextView)findViewById(R.id.driver_name);
        vehicleyear=(TextView)findViewById(R.id.vehicle_year);
        vehiclemodel=(TextView)findViewById(R.id.vehicle_model);
        vehiclenumber=(TextView)findViewById(R.id.vehiclenumber);
        vehicleImage=(ImageView)findViewById(R.id.vehicleImage);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        ratingVal=(RatingBar)findViewById(R.id.rtbProductRating);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        progressDialog=new ProgressDialog(Driver_detail.this);
        imageLoader=ImageLoader.getInstance();

        imageLoader.init(ImageLoaderConfiguration.createDefault(Driver_detail.this));
        options = new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.blank).cacheOnDisc().cacheInMemory().build();

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        //  navigation_btn=(ImageView)findViewById(R.id.navigation_btn);
        setupDrawerContent(nvDrawer);
        toolbar.setNavigationIcon(R.drawable.navigation);
        toolbar.setTitle("MUV");
        toolbar.setTitleTextColor(Color.GRAY);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });

        hashme=new ArrayList<>();
        position=getIntent().getExtras().getInt("position");
        Intent intent=getIntent();
        hashme = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("hashme");
        //sharedpreferences.getString("galleryImages",galleryImages);

        Bundle extras = getIntent().getExtras();
       pickaddress = extras.getString("pickaddress");
        dropaddress = extras.getString("dropaddress");
        truckTypeimg = extras.getString("truckType");
        dollar = extras.getInt("dollar");
        passengerval = extras.getInt("passengerval");
        lat_pick = extras.getDouble("lat_pick");
        lon_pick = extras.getDouble("lon_pick");
        lat_drop = extras.getDouble("lat_drop");
        lon_drop = extras.getDouble("lon_drop");
        dist = extras.getDouble("dist");
        selectedImage=extras.getString("selectedImage");
        bookbtn_value=extras.getString("book_btnv");
       distanc=String.format("%.2f", dist);
        Log.d("selectedImage", "detailselectedImage" + selectedImage);
        Log.d("Addres", "detailpickaddress" + pickaddress);
        Log.d("Addres", "detaildropaddress" + dropaddress);
        Log.d("lat_picklat_pick","lat_pick"+lat_pick+"-lon:-"+lon_pick);
        Log.d("lon_pick","lon_pick"+lat_drop+"-lon:-"+lon_drop);
        Log.d("dollar", "dollar" + dollar);
        Log.d("distanc", "distanc" + distanc);
        Log.d("passengerval", "passengerval" + passengerval);
        if(bookbtn_value.equals("book_now")){
            bookLater=0;
        }
        else{
            bookLater=1;
        }
        if(truckTypeimg.equals("pickup")){
            vehicleImage.setBackgroundResource(R.drawable.pickupp);
//            imageLoader.displayImage("drawable://" + R.drawable.pickup_1, vehicleImage, options);
        }
        else if(truckTypeimg.equals("vans")){
            vehicleImage.setBackgroundResource(R.drawable.vansp);
//            imageLoader.displayImage("drawable://" + R.drawable.van, vehicleImage, options);
        }
        else{
            vehicleImage.setBackgroundResource(R.drawable.boxtruck2);
//            imageLoader.displayImage("drawable://" + R.drawable.boxtruck2, vehicleImage, options);
        }
        try {
            Uri SelectedImage=Uri.parse(selectedImage);
            bitmap = new UserPicture(SelectedImage, getContentResolver()).getBitmap();
            Log.d("bitmapImage","bitmapImage"+bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(hashme.get(position).get("rating").equals("") ){
        }else{
            ratingVal.setRating(Float.parseFloat(hashme.get(position).get("rating")));
        }
        driverId=hashme.get(position).get("user_id");
        phonenum.setText(hashme.get(position).get("driver_phone"));
        drivername.setText(hashme.get(position).get("drivername"));
        vehicleyear.setText(hashme.get(position).get("vehicle_year"));
        vehiclemodel.setText(hashme.get(position).get("vehicle_model"));
        vehiclenumber.setText(hashme.get(position).get("vehicle_number"));
        book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                try {
                    pickaddress = extras.getString("pickaddress");
                } catch (Exception e) {
                }
                if ((pickaddress == null) || (pickaddress.equals(""))) {
                    //Put up the Yes/No message box
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(Driver_detail.this);
                    alertDialog.setTitle("MUV");
                    alertDialog.setMessage("You have to choose Pick Up Address for booking!");
//                        alertDialog.setIcon(R.drawable.add_icon);


                    alertDialog.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Write your code here to execute after dialog
                                    Intent intent = new Intent(Driver_detail.this, Twoautocompletetetbox.class);
                                    startActivity(intent);
                                    overridePendingTransition(0, 0);
                                    finish();
//                                        Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                                    // tv.setText("Yes Button clicked");
                                }
                            });
                    alertDialog.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //  Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                                    dialog.cancel();
                                    //  tv.setText("No Button clicked");
                                }
                            });

                    alertDialog.show();
//                        new ShowMsg().createDialog(SellingList.this, "Oops! no seller in your area Be the first?");
                } else {
                    Booknow();
                }
            }
        });
        phonenum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phonenum.getText().toString().trim()));//change the number
                startActivity(callIntent);
            }
        });

    }

    public void setupDrawerContent(NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(

                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override

                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        selectDrawerItem(menuItem);

                        return true;

                    }

                });

    }
    public void selectDrawerItem(MenuItem menuItem) {

        //Fragment fragment = null;
        //Class fragmentClass;

        switch(menuItem.getItemId()) {

            case R.id.nav_first_fragment:
                Intent intent=new Intent(Driver_detail.this,First_Fragment.class);
                startActivity(intent);
                // fragmentClass = FirstFragment.class;

                break;

            case R.id.nav_second_fragment:
                Intent intent1=new Intent(Driver_detail.this,Second_Fragment.class);
                startActivity(intent1);
                //  fragmentClass = Second_Fragment.class;

                break;

////            case R.id.nav_third_fragment:
//
//                // fragmentClass = Third_Fragment.class;
//
//                break;

            default:
                Intent intent11=new Intent(Driver_detail.this,First_Fragment.class);
                startActivity(intent11);
                //  fragmentClass = Third_Fragment.class;

        }






        setTitle(menuItem.getTitle());

        mDrawer.closeDrawers();

    }

    private void Booknow(){
        progressDialog.setMessage("Loading...");
    progressDialog.setCancelable(false);
    progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Toast.makeText(Driver_detail.this,response,Toast.LENGTH_LONG).show();
                        Log.d("sss", "sss" + response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            String status=jsonObject.getString("status");
                                String message=jsonObject.getString("Message");
                                if(status.equalsIgnoreCase("true")) {
                                    new ShowMsg().createDialog(Driver_detail.this, message);
                                    Intent intent=new Intent(Driver_detail.this,Book_thanks.class);
                                    intent.putExtra("bookbtn", bookbtn_value);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(0,0);
                                }else{
                                    new ShowMsg().createDialog(Driver_detail.this, "Oops! Vehicle is not available ");
                                    // no_found.setVisibility(View.VISIBLE);
                                }


                        }catch (Exception e){
                            Log.d("eeeee:", "eeeee" + e);
                        }
                        progressDialog.dismiss();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Driver_detail.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                ByteArrayOutputStream arrayOutputStream=new ByteArrayOutputStream();

                if(bitmap==null){

                }else {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, arrayOutputStream);
                    byte[] data = arrayOutputStream.toByteArray();
                    file = Base64.encodeToString(data, Base64.DEFAULT);
                    Log.d("fileImage:", "fileImage" + file);
                }
                Log.d("userId","userId"+sharedpreferences.getString(uUid,""));
            params.put("logged_id", sharedpreferences.getString(uUid, ""));
            params.put("driver_id", driverId);
            params.put("fair", String.valueOf(dollar)+"usd");
            params.put("loc_name", pickaddress);
            params.put("droplocation", dropaddress);
            params.put("no_of_pas", String.valueOf(passengerval));
            params.put("distance", distanc+"miles");
            params.put("pick_up_lat", String.valueOf(lat_pick));
            params.put("pick_up_lon", String.valueOf(lon_pick));
            params.put("drop_off_lat", String.valueOf(lat_drop));
            params.put("drop_off_lon", String.valueOf(lon_drop));
            params.put("book_later", String.valueOf(bookLater));
                if(file==null){
//
                }else {

                    params.put("profile_name", "" + new Date().getTime() + ".png");
                    params.put("profile_file", file);
                }
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                mDrawer.openDrawer(GravityCompat.START);

                return true;

        }



        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        //Include the code here


//        if(pickaddress.equals("")) {
//            Intent intent = new Intent(Driver_detail.this, Vehicle_type.class);
//            startActivity(intent);
//            finish();
//            overridePendingTransition(0, 0);
//        }
//        else{
            Intent intent = new Intent(Driver_detail.this, Vehicle_type.class);
            Log.d("Addres", "typepickaddress" + pickaddress);
            Log.d("Addres", "typedropaddress" + dropaddress);
            Log.d("lat_picklat_pick", "lat_pick" + lat_pick + "-lon:-" + lon_pick);
            Log.d("lon_pick", "lon_pick" + lat_drop + "-lon:-" + lon_drop);
            Log.d("dollar", "dollar" + dollar);
            Log.d("passengerval", "passengerval" + passengerval);
            intent.putExtra("pickaddress", pickaddress);
            intent.putExtra("dropaddress", dropaddress);
            intent.putExtra("dollar", dollar);
            intent.putExtra("passengerval", passengerval);
            intent.putExtra("lat_pick", lat_pick);
            intent.putExtra("lon_pick", lon_pick);
            intent.putExtra("lat_drop", lat_drop);
            intent.putExtra("lon_drop", lon_drop);
            intent.putExtra("dist", dist);
            startActivity(intent);
            overridePendingTransition(0, 0);
        finish();
//        }

    }
}
