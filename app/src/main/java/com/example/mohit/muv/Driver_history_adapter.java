package com.example.mohit.muv;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by admin on 21-03-2016.
 */
public class Driver_history_adapter extends BaseAdapter {
    Context c1;

    public Driver_history_adapter(Driver_history driver_history) {
        c1=driver_history;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=LayoutInflater.from(c1);
        convertView=inflater.inflate(R.layout.driver_history_list,null);
        TextView text=(TextView)convertView.findViewById(R.id.history);
        text.setText("History");

        return convertView;
    }
}
