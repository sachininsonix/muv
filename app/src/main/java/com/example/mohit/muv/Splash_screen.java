package com.example.mohit.muv;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mohit.muv.LoginRegister.Login;
import com.example.mohit.muv.LoginRegister.Register;
import com.example.mohit.muv.constant.Iconstant;

import java.util.logging.Handler;

/**
 * Created by mohit on 22-Feb-16.
 */
public class Splash_screen extends Activity implements Iconstant {
    /** Duration of wait **/

    ImageView tvMuvText;
    SharedPreferences preferences;
    GPSTracker gpsTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        gpsTracker = new GPSTracker(Splash_screen.this);
        final NetworkInfo activeNetwork;
        ConnectivityManager cm =
                (ConnectivityManager) Splash_screen.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        activeNetwork = cm.getActiveNetworkInfo();
        final boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        preferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        tvMuvText = (ImageView) findViewById(R.id.tvMuvText);
        Animation myanimation = AnimationUtils.loadAnimation(this, R.anim.myanimation);
        tvMuvText.startAnimation(myanimation);
//
//
        if (isConnected == true) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(4000);
                        if (preferences.getString(uUid, "").equals("")) {
                            Intent i = new Intent(Splash_screen.this, Login.class);
                            startActivity(i);

                            //Remove activity
                            finish();
                        } else {


                            Intent i = new Intent(Splash_screen.this, Twoautocompletetetbox.class);
                            startActivity(i);

                            //Remove activity
                            finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();

        }

    else {
            Toast.makeText(Splash_screen.this, " No Internet connection ", Toast.LENGTH_LONG).show();
        }}

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }
}
