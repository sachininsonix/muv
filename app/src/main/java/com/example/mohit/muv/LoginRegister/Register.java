package com.example.mohit.muv.LoginRegister;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.mohit.muv.R;
import com.example.mohit.muv.Validation.Register_validation;
import com.example.mohit.muv.constant.Iconstant;

import org.json.JSONObject;

import java.net.URLEncoder;

/**
 * Created by mohit on 18-Feb-16.
 */
public class Register extends Activity implements Iconstant{
    public static  EditText etUserName,etFirstName,etLastName, etPassword,etCPassword,etEmail,etPhoneNum,agreeerror;
    public static  CheckBox cbIsAgree,is_newsletter;
    public static  Button btnRegister;
    android.app.ProgressDialog progressDialog;
    public static int is_newsletterFlag = 0;
    public static String d_type,d_make,d_model,d_year,d_vehicleNumber;
    public static int driver_info = 0;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        progressDialog=new ProgressDialog(Register.this);

        etFirstName = (EditText)findViewById(R.id.etFirstName);
        etLastName = (EditText)findViewById(R.id.etLastName);
        etUserName = (EditText)findViewById(R.id.etUserName);
        preferences=getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor= preferences.edit();
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        etPassword = (EditText)findViewById(R.id.etPassword);
        etCPassword = (EditText)findViewById(R.id.etCPassword);
        etEmail = (EditText)findViewById(R.id.etEmail);
        etPhoneNum = (EditText)findViewById(R.id.etPhoneNum);
        agreeerror = (EditText)findViewById(R.id.agreeerror);
        cbIsAgree = (CheckBox)findViewById(R.id.cbIsAgree);
        is_newsletter = (CheckBox)findViewById(R.id.is_newsletter);
        btnRegister = (Button)findViewById(R.id.btnRegister);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Register_validation rt = new Register_validation(Register.this);
//                rt.register_validation();
                final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                        if (isNetworkAvailable() == false) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setMessage("Error in Network Connection")
                                    .setIcon(R.drawable.alert)
                                    .setCancelable(false)
                                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //  Action for 'NO' Button
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert = builder.create();
                            //Setting the title manually
                            alert.setTitle("Alert");
                            alert.show();
                            //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
                        }
                        else if( etFirstName.getText().toString().length() == 0 ) {
                            etFirstName.setError("First Name is required!");

                        }
                        else if( etUserName.getText().toString().length() == 0 ) {
                            etUserName.setError("Username is required!");

                        }
                        else if(etPassword.getText().toString().length() == 0 ) {
                            etPassword.setError("Password is required!");
                            etPassword.requestFocus();
                        }
                        else if( etCPassword.getText().toString().length() == 0 ) {
                            etCPassword.setError("Confirm Password is required!");
                            etCPassword.requestFocus();
                        }
                        else if(!etPassword.getText().toString().matches(etCPassword.getText().toString())) {
                            etPassword.setError("Password is mismatch!");
                            etCPassword.setError("Confirm Password is mismatch!");
                            etPassword.requestFocus();
                        }
                        else if( etEmail.getText().toString().length() == 0 ) {
                            etEmail.setError("Email is required!");
                            etEmail.requestFocus();
                        }
                        else if(!etEmail.getText().toString().matches(emailPattern)){
                            etEmail.setError("Invaild email!");
                            etEmail.requestFocus();
                        }
                        else if( etPhoneNum.getText().toString().length() == 0 ) {
                            etPhoneNum.setError("Phone is required!");
                            etPhoneNum.requestFocus();
                        }
                        else if( etPhoneNum.getText().toString().length() < 10 || etPhoneNum.getText().toString().length() > 10) {
                            etPhoneNum.setError("Invaild Phone number!");
                            etPhoneNum.requestFocus();
                        }
                        else if(!cbIsAgree.isChecked()){
                            agreeerror.setError("Please select agree");
                        }
                        else {
                            if(is_newsletter.isChecked()){
                                is_newsletterFlag = 1;
                            }
                            processRegister();
                        }


                Register.etPassword.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        etPassword.setError(null);
                        etCPassword.setError(null);
                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }
                    @Override
                    public void afterTextChanged(Editable s) {
                        etPassword.setError(null);
                        etCPassword.setError(null);
                    }
                });
                Register.etCPassword.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        etPassword.setError(null);
                        etCPassword.setError(null);
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        etPassword.setError(null);
                        etCPassword.setError(null);
                    }
                });
                cbIsAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            agreeerror.setError(null);

                        } else {
                            agreeerror.setError("Please select agree");
                        }
                    }
                });
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    private void processRegister(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Intent rg = getIntent();
        d_type = rg.getStringExtra("d_type");
        d_year = rg.getStringExtra("d_year");
        d_make = rg.getStringExtra("d_make");
        d_model = rg.getStringExtra("d_model");
        d_vehicleNumber = rg.getStringExtra("d_vehicleNumber");
        driver_info = rg.getIntExtra("driver_info",0);
        String androidId = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);
        String url="";
        if(driver_info == 1){
            url = "http://insonix.com/design.insonix.com/muv/register.php?username="+URLEncoder.encode(etUserName.getText().toString().trim())+"&firstname="+etFirstName.getText().toString().trim()+"&lastname="+etLastName.getText().toString().trim()+"&email="+etEmail.getText().toString().trim()+"&password="+etPassword.getText().toString().trim()+"&phone="+etPhoneNum.getText().toString().trim()+"&news="+""+"&device_id="+preferences.getString("reg_id","")+"&driver_info="+driver_info+"&model="+ URLEncoder.encode(d_model.trim())+"&make="+URLEncoder.encode(d_make.trim()) + "&year=" + d_year +  "&vehicle_number=" + URLEncoder.encode(d_vehicleNumber) +"&type=" + URLEncoder.encode(d_type.trim());
        }else{
            url = "http://insonix.com/design.insonix.com/muv/register.php?username="+URLEncoder.encode(etUserName.getText().toString().trim())+"&firstname="+etFirstName.getText().toString().trim()+"&lastname="+etLastName.getText().toString().trim()+"&email="+etEmail.getText().toString().trim()+"&password="+etPassword.getText().toString().trim()+"&phone="+etPhoneNum.getText().toString().trim()+"&news="+is_newsletterFlag+"&device_id="+preferences.getString("reg_id","");
        }
        Log.d("url:", "urlregister" + url);
        //Log.e("Url : ",url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.d("register:", "register" + response.toString());

                try{
                    String status=response.getString("status");
                    String message=response.getString("message");
                    if(status.equals("ok")) {
                        //{"mail":"Your have successfully entered","email":"amanwes@gmail.com","username":"amanas","phone":"1212323","uid":"2","status":"ok"}

                        Toast.makeText(Register.this,"Registered Successfully",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Register.this,Login.class);
                        startActivity(intent);
//                        nochat.setVisibility(View.GONE);
                    }else{
                        Toast.makeText(Register.this,message,Toast.LENGTH_LONG).show();
//                        nochat.setVisibility(View.VISIBLE);
//                        nochat.setBackgroundResource(R.drawable.chat_mai);
                    }

                }catch(Exception e){
                    Log.d("eeee:", "eee" + e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
//        String url = "http://52.35.22.61/web/register.php?username="+etUserName.getText().toString()+"&etPassword="+etPassword.getText().toString()+"&email="+etEmail.getText().toString()+"&phone_number="+etPhoneNum.getText().toString();
//        //Toast.makeText(Register.this, "Enter", Toast.LENGTH_LONG).show();
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        });
//        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
