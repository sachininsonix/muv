package com.example.mohit.muv;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.mohit.muv.LoginRegister.AppController;
import com.example.mohit.muv.Navigation_Drawer.First_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Second_Fragment;
import com.example.mohit.muv.constant.Iconstant;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by insonix on 1/4/16.
 */
public class DriverMap extends Activity implements Iconstant{

    static public Marker marker;
    ImageView navigation_btn;
    static public Marker marker1;
    double lat_pick,lon_pick,lat_drop,lon_drop;
    ImageView select_image;
    TextView passenger1, passenger2, passenger3, passenger4;
    String sp[];
    private static final int GOOGLE_API_CLIENT_ID = 0;
    GoogleApiClient mGoogleApiClient;
    static public GoogleMap googleMap;
    DrawerLayout mDrawer;
    LatLng latLng,latLng1;
    NavigationView nvDrawer;
    ro1 ro11;
    Toolbar toolbar;
    Button startride,endride,btn_Rateus;
    TextView edt_pick_up,fare_estaimate;
    ProgressDialog progressDialog;
    String url;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    LinearLayout button_lay;
    String loc_name,pick_up_lat,pick_up_lon,drop_off_lat,drop_off_lon,message,booking_id,ride_id,
            totdistance,numberOnly,aplhabetonly,user_id,RatingValue;
    Double TotalFare;
    ImageView btnClosePopup;
    LinearLayout popup_element,rate_element;
    FrameLayout flContent;
    EditText feed_descript;
    RatingBar ratingBar;
    ArrayList<LatLng> markerPoints;
    String[] distanceaftersplit;
//    private static final String TAG = "paymentExample";
//    /**
//     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
//     *
//     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
//     * from https://developer.paypal.com
//     *
//     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
//     * without communicating to PayPal's servers.
//     */
//    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
//    // note that these credentials will differ between live & sandbox environments.
//    private static final String CONFIG_CLIENT_ID = "AZ4hAFXudZ5PTeH8PtrrO1sPJnHYnex0I8WTuSyDJshS5BIoybqs1uZL28zsuT8LRs3A9m74pWxX7_3o";
//    private static final int REQUEST_CODE_PAYMENT = 1;
//    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
//    private static final int REQUEST_CODE_PROFILE_SHARING = 3;
//    private static PayPalConfiguration config = new PayPalConfiguration()
//            .environment(CONFIG_ENVIRONMENT)
//            .clientId(CONFIG_CLIENT_ID)
//// The following are only used in PayPalFuturePaymentActivity.
//            .merchantName("Example Merchant")
//            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
//            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    // The method that displays the popup.
    private PopupWindow pwindo;
//    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
//            new LatLng(28.70, -127.50), new LatLng(48.85, -55.19));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_map);
        progressDialog=new ProgressDialog(DriverMap.this);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        preferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        Bundle extras = getIntent().getExtras();
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        startride=(Button)findViewById(R.id.startride);
        endride=(Button)findViewById(R.id.endride);
        edt_pick_up=(TextView)findViewById(R.id.edt_pick_up);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        button_lay=(LinearLayout)findViewById(R.id.button_lay);
        popup_element=(LinearLayout)findViewById(R.id.popup_element);
        rate_element=(LinearLayout)findViewById(R.id.rate_element);
        fare_estaimate=(TextView)findViewById(R.id.Fare_estimate);
        btnClosePopup = (ImageView)findViewById(R.id.btn_close_popup);
        //  navigation_btn=(ImageView)findViewById(R.id.navigation_btn);
        setupDrawerContent(nvDrawer);
        toolbar.setNavigationIcon(R.drawable.navigation);
        toolbar.setTitle("MUV");
        toolbar.setTitleTextColor(Color.GRAY);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });
        // Initializing
        markerPoints = new ArrayList<LatLng>();
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        googleMap = mapFragment.getMap();
        try{
            message=extras.getString("message");
            Log.d("ddd",""+message);
            totdistance=extras.getString("distance");
            Log.d("DistanceMap","DistanceMap"+totdistance);
            pick_up_lat=extras.getString("pick_up_lat");
            pick_up_lon=extras.getString("pick_up_lon");
            booking_id=extras.getString("booking_id");
            lat_pick=Double.parseDouble(pick_up_lat);
            lon_pick=Double.parseDouble(pick_up_lon);
            latLng=new LatLng(lat_pick,lon_pick);
            Log.d("ffff","fffff"+latLng);
            drop_off_lat=extras.getString("drop_off_lat");
            drop_off_lon=extras.getString("drop_off_lon");
            loc_name=extras.getString("loc_name");
            user_id=extras.getString("user_id");
            lat_drop=Double.parseDouble(drop_off_lat);
            lon_drop=Double.parseDouble(drop_off_lon);
            Log.d("lat_pick","lat_pick"+lat_pick+"lon pick"+lon_pick);
        }catch (Exception e){
            e.printStackTrace();
        }
//        Intent intent = new Intent(this, PayPalService.class);
//        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
//        startService(intent);
        if(message.equals("Driver Accepted")){
            button_lay.setVisibility(View.GONE);
            edt_pick_up.setText("Driver's Current Location");
        }
        else{
            startride.setVisibility(View.VISIBLE);
            edt_pick_up.setText("User PickUp Location");
        }
        Log.d("lat_pick","lat_pick"+lat_pick);
        Log.d("lon_drop","lon_drop"+lon_drop);
//        latLng1=new LatLng(lat_drop,lon_drop);
        marker = googleMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                        // Anchors the marker.png on the bottom left
                .position(new LatLng(lat_pick, lon_pick)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));
        LatLng origin =  new LatLng(lat_pick, lon_pick);;
        LatLng dest =   new LatLng(lat_drop, lon_drop);;
        Log.d("origin","origin"+origin);
        Log.d("dest","dest"+dest);
//        // Getting URL to the Google Directions API
//        String url = getDirectionsUrl(origin, dest);
//
//        DownloadTask downloadTask = new DownloadTask();
//
//        // Start downloading json data from Google Directions API
//        downloadTask.execute(url);



        startride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ridestarted();
            }
        });
        endride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                button_lay.setVisibility(View.GONE);
                RideEnd();
            }
        });
    }
//    private String getDirectionsUrl(LatLng origin,LatLng dest){
//
//        // Origin of route
//        String str_origin = "origin="+origin.latitude+","+origin.longitude;
//
//        // Destination of route
//        String str_dest = "destination="+dest.latitude+","+dest.longitude;
//
//
//        // Sensor enabled
//        String sensor = "sensor=false";
//
//        // Building the parameters to the web service
//        String parameters = str_origin+"&"+str_dest+"&"+sensor;
//
//        // Output format
//        String output = "json";
//
//        // Building the url to the web service
//        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
//
//
//        return url;
//    }
//
//    /** A method to download json data from url */
//    private String downloadUrl(String strUrl) throws IOException{
//        String data = "";
//        InputStream iStream = null;
//        HttpURLConnection urlConnection = null;
//        try{
//            URL url = new URL(strUrl);
//
//            // Creating an http connection to communicate with url
//            urlConnection = (HttpURLConnection) url.openConnection();
//            // Connecting to url
//            urlConnection.connect();
//            // Reading data from url
//            iStream = urlConnection.getInputStream();
//            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
//            StringBuffer sb  = new StringBuffer();
//            String line = "";
//            while( ( line = br.readLine())  != null){
//                sb.append(line);
//            }
//            data = sb.toString();
//            br.close();
//        }catch(Exception e){
//            Log.d("Exception while downloading url", e.toString());
//        }finally{
//            iStream.close();
//            urlConnection.disconnect();
//        }
//        return data;
//    }
//
//
//
//    // Fetches data from url passed
//    private class DownloadTask extends AsyncTask<String, Void, String>{
//
//        // Downloading data in non-ui thread
//        @Override
//        protected String doInBackground(String... url) {
//
//            // For storing data from web service
//            String data = "";
//
//            try{
//                // Fetching the data from web service
//                data = downloadUrl(url[0]);
//            }catch(Exception e){
//                Log.d("Background Task",e.toString());
//            }
//            return data;
//        }
//
//        // Executes in UI thread, after the execution of
//        // doInBackground()
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//
//            ParserTask parserTask = new ParserTask();
//
//            // Invokes the thread for parsing the JSON data
//            parserTask.execute(result);
//
//        }
//    }
//
//    /** A class to parse the Google Places in JSON format */
//    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
//
//        // Parsing the data in non-ui thread
//        @Override
//        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
//
//            JSONObject jObject;
//            List<List<HashMap<String, String>>> routes = null;
//
//            try{
//                jObject = new JSONObject(jsonData[0]);
//                DirectionsJSONParser parser = new DirectionsJSONParser();
//
//                // Starts parsing data
//                routes = parser.parse(jObject);
//            }catch(Exception e){
//                e.printStackTrace();
//            }
//            return routes;
//        }
//
//        // Executes in UI thread, after the parsing process
//        @Override
//        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
//            ArrayList<LatLng> points = null;
//            PolylineOptions lineOptions = null;
//            MarkerOptions markerOptions = new MarkerOptions();
//            String distance = "";
//            String duration = "";
//            String CurrentString ;
//            String TotalMinutes;
//
//
//            if(result.size()<1){
//                Toast.makeText(getBaseContext(), "No Points", Toast.LENGTH_SHORT).show();
//                return;
//            }
//
//
//            // Traversing through all the routes
//            for(int i=0;i<result.size();i++){
//                points = new ArrayList<LatLng>();
//                lineOptions = new PolylineOptions();
//
//                // Fetching i-th route
//                List<HashMap<String, String>> path = result.get(i);
//
//                // Fetching all the points in i-th route
//                for(int j=0;j<path.size();j++){
//                    HashMap<String,String> point = path.get(j);
//
//                    if(j==0){    // Get distance from the list
//                        distance = (String)point.get("distance");
//                        continue;
//                    }else if(j==1){ // Get duration from the list
//                        duration = (String)point.get("duration");
//                        continue;
//                    }
//
//                    double lat = Double.parseDouble(point.get("lat"));
//                    double lng = Double.parseDouble(point.get("lng"));
//                    LatLng position = new LatLng(lat, lng);
//
//                    points.add(position);
//                }
//
//                // Adding all the points in the route to LineOptions
//                lineOptions.addAll(points);
//                lineOptions.width(2);
//                lineOptions.color(Color.RED);
//
//            }
//
////            tvDistanceDuration.setText("Distance:"+distance + ", Duration:"+duration);
//
//            // Drawing polyline in the Google Map for the i-th route
//            googleMap.addPolyline(lineOptions);
//            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        /*
//         * for (Marker marker : markers) {
//         * builder.include(marker.getPosition()); }
//         */
//            for (LatLng point : points) {
//                builder.include(point);
//            }
//
//            LatLngBounds bounds = builder.build();
//            int padding = 200; // offset from edges of the map in pixels
//            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
//                    padding);
//           googleMap.moveCamera(cu);
//           googleMap.animateCamera(cu, 2000, null);
//        }
//    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //        private void drawMarker(LatLng point) {
//            mMarkerPoints.add(point);
//
//            // Creating MarkerOptions
//            MarkerOptions options = new MarkerOptions();
//
//            // Setting the position of the marker.png
//            options.position(point);
//
//
//            if (mMarkerPoints.size() == 1) {
//                options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
//            } else if (mMarkerPoints.size() == 2) {
//                options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
//            }
//
//            // Add new marker.png to the Google Map Android API V2
//            googleMap.addMarker(options);
//        }
//
//        @Override
//        public void onLocationChanged(Location location) {
//            // Draw the marker.png, if destination location is not set
//            if (mMarkerPoints.size() < 2) {
//
//                mLatitude = location.getLatitude();
//                mLongitude = location.getLongitude();
//                LatLng point = new LatLng(mLatitude, mLongitude);
//
//                googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
//                googleMap.animateCamera(CameraUpdateFactory.zoomTo(7));
//
//                drawMarker(point);
//            }
//
//        }
//
//        @Override
//        public void onProviderDisabled(String provider) {
//            // TODO Auto-generated method stub
//        }
//
//        @Override
//        public void onProviderEnabled(String provider) {
//            // TODO Auto-generated method stub
//        }
//
//        @Override
//        public void onStatusChanged(String provider, int status, Bundle extras) {
//            // TODO Auto-generated method stub
//        }
//    }
//
//
    class DirectionsJSONParser {

        /**
         * Receives a JSONObject and returns a list of lists containing latitude and longitude
         */
        public List<List<HashMap<String, String>>> parse(JSONObject jObject) {

            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;

            try {

                jRoutes = jObject.getJSONArray("routes");

                /** Traversing all routes */
                for (int i = 0; i < jRoutes.length(); i++) {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                    List path = new ArrayList<HashMap<String, String>>();

                    /** Traversing all legs */
                    for (int j = 0; j < jLegs.length(); j++) {
                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                        /** Traversing all steps */
                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline = "";
                            polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                            List<LatLng> list = decodePoly(polyline);

                            /** Traversing all points */
                            for (int l = 0; l < list.size(); l++) {
                                HashMap<String, String> hm = new HashMap<String, String>();
                                hm.put("lat", Double.toString(((LatLng) list.get(l)).latitude));
                                hm.put("lng", Double.toString(((LatLng) list.get(l)).longitude));
                                path.add(hm);
                            }
                        }
                        routes.add(path);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }


            return routes;
        }


        /**
         * Method to decode polyline points
         * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
         */
        private List<LatLng> decodePoly(String encoded) {

            List<LatLng> poly = new ArrayList<LatLng>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }

            return poly;
        }
    }
    public void setupDrawerContent(NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(

                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override

                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        selectDrawerItem(menuItem);

                        return true;

                    }

                });

    }
    public void selectDrawerItem(MenuItem menuItem) {

       switch(menuItem.getItemId()) {

            case R.id.nav_first_fragment:
                Intent intent=new Intent(DriverMap.this,First_Fragment.class);
                startActivity(intent);
                break;
            case R.id.nav_second_fragment:
                Intent intent1=new Intent(DriverMap.this,Second_Fragment.class);
                startActivity(intent1);
                break;

//            case R.id.nav_third_fragment:
//
//                // fragmentClass = Third_Fragment.class;
//
//                break;

            default:
                Intent intent11=new Intent(DriverMap.this,First_Fragment.class);
                startActivity(intent11);
                //  fragmentClass = Third_Fragment.class;
        }
        setTitle(menuItem.getTitle());
        mDrawer.closeDrawers();
    }
    public void Ridestarted(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = "http://insonix.com/design.insonix.com/muv/booking_start.php?booking_id="+booking_id;
        Log.d("Url", "url" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    String message=jsonObject.getString("message");
                    if(status.equalsIgnoreCase("true")) {

                        ride_id=jsonObject.getString("ride_id");
                        endride.setVisibility(View.VISIBLE);
                        startride.setVisibility(View.GONE);
                    }else{
                        new ShowMsg().createDialog(DriverMap.this, message);
                        // no_found.setVisibility(View.VISIBLE);
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    public void RideEnd(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = "http://insonix.com/design.insonix.com/muv/booking_end.php?booking_id="+booking_id+"&ride_id="+ride_id;
        Log.d("Url", "url" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    if(status.equalsIgnoreCase("true")) {
                        ride_id=jsonObject.getString("ride_id");
                        double minute=jsonObject.getDouble("minute");
                        Log.d("totdistance","totdistance"+totdistance);
//                        String numdistan[]=totdistance.split("miles");
//                        Double numDistance  = Double.parseDouble(totdistance.replaceAll("[^0-9]", ""));
                        distanceaftersplit = totdistance.split("miles");
                        String ditancesplit=distanceaftersplit[0];
//                        distanceaftersplit[1];
                        Double numDistance  = Double.parseDouble(ditancesplit);
                        Log.d("numDistance","numDistance"+numDistance);
                        Log.d("minute","minute"+minute);
                        aplhabetonly = totdistance.replaceAll("[^A-Za-z]+", "");
                        TotalFare=10+(numDistance * 1.3)+(0.2 * minute );
                        String result = String.format("%.2f", TotalFare);
                        String FinalFare= result+" Usd";
                        Log.d("TotalFare", "result" + result);
                        try {

                            popup_element.setVisibility(View.VISIBLE);
// We need to get the instance of the LayoutInflater
//                            LayoutInflater inflater = (LayoutInflater) DriverMap.this
//                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                            View layout = inflater.inflate(R.layout.show_popup,
//                                    (ViewGroup) findViewById(R.id.popup_element));
//                            pwindo = new PopupWindow(layout, 600,400 , true);
//                            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
                            if(!FinalFare.equals("")){
                                fare_estaimate.setText(FinalFare);
                            }

                            btnClosePopup.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
//                                    onBuyPressed();
                                  RateUs();

                                }
                            });

//                            btnClosePopup.setOnClickListener(cancel_button_click_listener);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        initiatePopupWindow();
//                        new ShowMsg().createDialog(DriverMap.this, "Ride Id:-" + ride_id + " Time in minute:- " + minute+" Total fare:- "+result);
//                        Intent intent= new Intent(DriverMap.this,Driver_history.class);
//                        startActivity(intent);
//                        finish();

                    }else{
                        new ShowMsg().createDialog(DriverMap.this, "No record found");
                        // no_found.setVisibility(View.VISIBLE);
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
public void RateUs(){
        rate_element.setVisibility(View.VISIBLE);
    popup_element.setVisibility(View.GONE);
//    LayoutInflater inflater = (LayoutInflater) DriverMap.this
//            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    View layout = inflater.inflate(R.layout.feed_rating,
//            (ViewGroup) findViewById(R.id.popup_element));
//    pwindo = new PopupWindow(layout, 1000,600 , true);
//    pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
    ratingBar = (RatingBar)findViewById(R.id.ratingBar1);
    btn_Rateus = (Button)findViewById(R.id.btn_Rateus);
    feed_descript=(EditText)findViewById(R.id.desc);


    ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

            RatingValue=String.valueOf(ratingBar.getRating());
        }
    });



    btn_Rateus.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//                                    onBuyPressed();
//            Toast.makeText(DriverMap.this, String.valueOf(ratingBar.getRating()),Toast.LENGTH_SHORT).show();
                     SendFeedback();
        }
    });
}
    public void SendFeedback(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = "http://insonix.com/design.insonix.com/muv/save_rating.php?user_id="+user_id+"&booking_id="+booking_id+
                "&driver_id="+preferences.getString(uUid,"")+"&comments="+ URLEncoder.encode(feed_descript.getText().toString().trim())+"&points="+RatingValue;
        Log.d("Url", "url" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    String message=jsonObject.getString("message");
                    if(status.equalsIgnoreCase("true")) {
                        rate_element.setVisibility(View.GONE);
                        button_lay.setVisibility(View.GONE);
//                        new ShowMsg().createDialog(DriverMap.this, "Thanks to Use Our Servcies!");
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DriverMap.this);
                        alertDialog.setTitle("MUV");
                        alertDialog.setMessage("Thanks to Use Our Services! Stay Connected!");
//                        alertDialog.setIcon(R.drawable.add_icon);


                        alertDialog.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to execute after dialog
                                        Intent intent = new Intent(DriverMap.this, Driver_history.class);
                                        startActivity(intent);
                                        overridePendingTransition(0, 0);
                                        finish();
//                                        Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                                        // tv.setText("Yes Button clicked");
                                    }
                                });
//                        alertDialog.setNegativeButton("NO",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        //  Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
//                                        dialog.cancel();
//                                        //  tv.setText("No Button clicked");
//                                    }
//                                });

                        alertDialog.show();
                    }else{
                        new ShowMsg().createDialog(DriverMap.this, message);
                        // no_found.setVisibility(View.VISIBLE);
                    }
                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
//    public void onBuyPressed() {
///*
//* PAYMENT_INTENT_SALE will cause the payment to complete immediately.
//* Change PAYMENT_INTENT_SALE to
//* - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
//* - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
//* later via calls from your server.
//*
//* Also, to include additional payment details and an item list, see getStuffToBuy() below.
//*/
//        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
///*
//* See getStuffToBuy(..) for examples of some available payment options.
//*/
//        Intent intent = new Intent(DriverMap.this, PaymentActivity.class);
//// send the same configuration for restart resiliency
//        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
//        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
//        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
//    }
//    private PayPalPayment getThingToBuy(String paymentIntent) {
//        return new PayPalPayment(new BigDecimal("1.75"), "USD", "sample item",
//                paymentIntent);
//    }
//    /*
//    * This method shows use of optional payment details and item list.
//    */
//
//    public void onFuturePaymentPressed(View pressed) {
//        Intent intent = new Intent(DriverMap.this, PayPalFuturePaymentActivity.class);
//// send the same configuration for restart resiliency
//        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
//        startActivityForResult(intent, REQUEST_CODE_FUTURE_PAYMENT);
//    }
//
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == REQUEST_CODE_PAYMENT) {
//            if (resultCode == Activity.RESULT_OK) {
//                PaymentConfirmation confirm =
//                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
//                if (confirm != null) {
//                    try {
//                        Log.i("hello", confirm.toJSONObject().toString(4));
//                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
///**
// * TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
// * or consent completion.
// * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
// * for more details.
// *
// * For sample mobile backend interactions, see
// * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
// */
//                        Toast.makeText(
//                                getApplicationContext(),
//                                "PaymentConfirmation info received from PayPal", Toast.LENGTH_LONG)
//                                .show();
//                    } catch (JSONException e) {
//                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
//                    }
//                }
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//                Log.i(TAG, "The user canceled.");
//            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
//                Log.i(
//                        TAG,
//                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
//            }
//
//        } else if (resultCode == Activity.RESULT_CANCELED) {
//            Log.i("FuturePaymentExample", "The user canceled.");
//        } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
//            Log.i(
//                    "FuturePaymentExample",
//                    "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
//        }
//
//
//
//        else if (resultCode == Activity.RESULT_CANCELED) {
//            Log.i("ProfileSharingExample", "The user canceled.");
//        } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
//            Log.i(
//                    "ProfileSharingExample",
//                    "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
//        }
//    }
//
//
//
//    @Override
//    public void onDestroy() {
//// Stop service when done
//        stopService(new Intent(this, PayPalService.class));
//        super.onDestroy();
//    }


//    private View.OnClickListener cancel_button_click_listener = new View.OnClickListener() {
//        public void onClick(View v) {
//            pwindo.dismiss();
//
//
//        }
//    };


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(message.equals("Driver Accepted")){
            Intent intent=new Intent(DriverMap.this,Twoautocompletetetbox.class);
            startActivity(intent);
            finish();

        }
        else{

            Intent intent=new Intent(DriverMap.this,Driver_history.class);
            startActivity(intent);
            finish();
        }

    }
}

