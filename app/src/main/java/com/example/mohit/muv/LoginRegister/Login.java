package com.example.mohit.muv.LoginRegister;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.mohit.muv.Fragments.Dialog_toggle_btn;
import com.example.mohit.muv.GPSTracker;
import com.example.mohit.muv.Home;
import com.example.mohit.muv.LocationAddress;
import com.example.mohit.muv.R;
import com.example.mohit.muv.Twoautocompletetetbox;
import com.example.mohit.muv.constant.Iconstant;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by mohit on 22-Feb-16.
 */
public class Login extends Activity implements Iconstant {
    ProgressDialog progressDialog;
    EditText emailAddress,password;
    Button sign_in,fb_sign_in,sign_up,registerToBeDriver;
    TextView forget_password;
    private String regid = null;

    protected String SENDER_ID="125774750686";
    private GoogleCloudMessaging gcm=null;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    SharedPreferences.Editor editor;
    CallbackManager callbackManager;
    LoginButton loginButton;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    GPSTracker gpsTracker;
    SharedPreferences sharedpreferences;
    LocationAddress locationAddress;
    double latitude,longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login);

        callbackManager = CallbackManager.Factory.create();

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor=sharedpreferences.edit();
        progressDialog=new ProgressDialog(Login.this);
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        gpsTracker=new GPSTracker(Login.this);
        emailAddress = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);
        sign_in = (Button)findViewById(R.id.sign_in);
//        fb_sign_in = (Button)findViewById(R.id.fb_sign_in);
        sign_up = (Button)findViewById(R.id.sign_up);
        forget_password = (TextView)findViewById(R.id.forget_password);
        registerToBeDriver = (Button)findViewById(R.id.registerToBeDriver);

        loginButton = (LoginButton) findViewById(R.id.login_button);

        loginButton.setReadPermissions("public_profile email");

        if(checkPlayServices()){
            registerInBackground();
        }else{

        }


//        if (gpsTracker.canGetLocation()) {
////            double latitude = gpsTracker.getLatitude();
////            double longitude = gpsTracker.getLongitude();
////            String languageToLoad = "en";
////            Locale locale = new Locale(languageToLoad);
////            Locale.setDefault(locale);
////            Configuration config = new Configuration();
////            config.locale = locale;
////            getBaseContext().getResources().updateConfiguration(config,
////                    getBaseContext().getResources().getDisplayMetrics());
//            latitude = gpsTracker.getLatitude();
//            double longitude = gpsTracker.getLongitude();
//
//            Log.d("latitude:","latitude"+latitude);
//            Log.d("longitude:", "longitude" + longitude);
//            locationAddress = new LocationAddress();
//            locationAddress.getAddressFromLocation(latitude, longitude,
//                    getApplicationContext(), new GeocoderHandler());
//           // Log.d("locationAddress:", "locationAddress" + locationAddress);
//        } else {
//            gpsTracker.showSettingsAlert();
//        }
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Toast.makeText(Login.this,"Success",Toast.LENGTH_SHORT).show();
                // App code
               // Log.v("", "fblogin onSuccess"+loginResult.toString());
                if(AccessToken.getCurrentAccessToken() != null){
                    RequestData();
                    Toast.makeText(Login.this,"Login Successfully",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Login.this,Home.class);
                    startActivity(intent);
                }

            }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(Login.this,"cancel",Toast.LENGTH_SHORT).show();
                Log.v("", "fblogin onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast.makeText(Login.this,"Error",Toast.LENGTH_SHORT).show();
                Log.v("", "fblogin onError");
            }
        });

        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this,Register.class );
                overridePendingTransition(0,0);
                startActivity(i);
            }
        });

        registerToBeDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this,Driver_registeration.class );
                // i.putExtra("is_driver","yes");
                startActivity(i);
            }
        });


        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable() == false) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                    builder.setMessage("Error in Network Connection")
                            .setIcon(R.drawable.alert)
                            .setCancelable(false)
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle("Alert");
                    alert.show();
                    //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
                }

                else if(emailAddress.getText().toString().length() == 0){
                    emailAddress.setError("Email is required!");
                }
//                else if(!emailAddress.getText().toString().matches(emailPattern)){
//                    emailAddress.setError("Invaild email!");
//                }
                else if(password.getText().toString().length() == 0){
                    password.setError("Password is required!");
                }
                else{
                    login_Process();
                }
            }
        });



    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void login_Process(){

        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        String androidId = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);
        //String url = "http://52.35.22.61/muv/login.php?email="+emailAddress.getText().toString()+"&password="+password.getText().toString()+"&device_id="+androidId;
        String url = "http://insonix.com/design.insonix.com/muv/login.php?email="+emailAddress.getText().toString().trim()+"&password="+password.getText().toString().trim()+"&device_id="+sharedpreferences.getString("reg_id","");
       Log.d("url:","urllogin"+ url);

        final StringBuffer sb = new StringBuffer();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("res:", "resss" + response.toString());
                progressDialog.dismiss();
                try{
                    String status=response.getString("status");
                    String message=response.getString("message");
                    if(status.equals("ok")) {
                        //{"mail":"Your have successfully entered","email":"amanwes@gmail.com","username":"amanas","phone":"1212323","uid":"2","status":"ok"}
//                        String message=response.getString("message");
                        String email=response.getString("email");
                        String username=response.getString("username");
                        String uid=response.getString("uid");
                        String available=response.getString("available");
                        String driver_type=response.getString("driver_type");
                        Log.d("res:", "resss" + driver_type);
                        Toast.makeText(Login.this,message,Toast.LENGTH_SHORT).show();


                        editor.putString(uUsername, username);
                        editor.putString(uMessage, message);
                        editor.putString(uEmail, email);
                        editor.putString(uUid, uid);
                        editor.putString(uType, driver_type);


                        editor.commit();

                        if(driver_type.equals("1")){
                            Intent intent = new Intent(Login.this, Dialog_toggle_btn.class);
//                            Toast.makeText(Login.this,"available:-"+available,Toast.LENGTH_SHORT).show();
                            intent.putExtra("available", available);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(0,0);
                        }
                        else {
                            Intent intent = new Intent(Login.this, Twoautocompletetetbox.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(0,0);
                        }
                    }else{
                        Toast.makeText(Login.this,message,Toast.LENGTH_LONG).show();
                    }

                }catch(Exception e){
                    //Log.d("eeee:", "eee" + e);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void RequestData(){

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object,GraphResponse response) {

                JSONObject json = response.getJSONObject();
                //Log.e("responseee----",response.toString());
                try {
                    if(json != null){
                       // String text = "<b>Id:</b>"+json.getString("id")+"<b>Name :</b> "+json.getString("name")+"<br><br><b>Email :</b> "+json.getString("email")+"<br><br><b>Profile link :</b> "+json.getString("link");
                        //Log.e("Result", text);

                        SharedPreferences.Editor editor = sharedpreferences.edit();

                        editor.putString(uUsername, json.getString("name"));
                        editor.putString(uMessage, "Login Successfully");
                        editor.putString(uEmail, json.getString("email"));
                        editor.putString(fUid, json.getString("id"));
                        editor.commit();


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }
    @Override protected void onResume()
    {
        super.onResume();
        checkPlayServices();
    }
    private boolean checkPlayServices() {
        int resultcode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (resultcode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultcode)) {
                GooglePlayServicesUtil.getErrorDialog(resultcode, Login.this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {

            }
            return false;
        }
        return true;
    }
    public void  registerInBackground(){
        new AsyncTask<Void,Void,String>(){

            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging
                                .getInstance(getApplicationContext());
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Registration ID :" + regid;
                    Log.i("Registration id", regid);

                    editor.putString("reg_id", regid);
                    editor.commit();


                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (!TextUtils.isEmpty(regid)) {
                    // storeRegIdinSharedPref(getActivity(), regId, emailID);
						/*
						 * Toast.makeText( con,
						 * "Registered with GCM Server successfully.\n\n" + msg,
						 * Toast.LENGTH_SHORT).show();
						 */
                } else {
						/*
						 * Toast.makeText( con,
						 * "Reg ID Creation Failed.\n\nEither you haven't enabled Internet or GCM server is busy right now. Make sure you enabled Internet and try registering again after some time."
						 * + msg, Toast.LENGTH_LONG).show();
						 */
                }
            }
        }.execute(null, null, null);
    }
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
//                    String languageToLoad = "en";
//                Locale locale = new Locale(languageToLoad);
//                Locale.setDefault(locale);
//                Configuration config = new Configuration();
//                config.locale = locale;
//                getBaseContext().getResources().updateConfiguration(config,
//                        getBaseContext().getResources().getDisplayMetrics());
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}

