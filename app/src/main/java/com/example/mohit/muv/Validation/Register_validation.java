package com.example.mohit.muv.Validation;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.example.mohit.muv.LoginRegister.Register;
import com.example.mohit.muv.R;

/**
 * Created by mohit on 19-Feb-16.
 */
public class Register_validation {

    Context context;

    public Register_validation(Context context) {
        this.context = context;
    }

    public void register_validation(){
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        Register.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( Register.etUserName.getText().toString().length() == 0 ) {
                    Register.etUserName.setError("Username is required!");
                    Register.etUserName.setFocusable(true);

                }
                else if( Register.etPassword.getText().toString().length() == 0 ) {
                    Register.etPassword.setError("Password is required!");
                }
                else if( Register.etCPassword.getText().toString().length() == 0 ) {
                    Register.etCPassword.setError("Confirm Password is required!");
                }
                else if(!Register.etPassword.getText().toString().matches(Register.etCPassword.getText().toString())) {
                    Register.etPassword.setError("Password is mismatch!");
                    Register.etCPassword.setError("Confirm Password is mismatch!");
                }
                else if( Register.etEmail.getText().toString().length() == 0 ) {
                    Register.etEmail.setError("Email is required!");
                }
                else if(!Register.etEmail.getText().toString().matches(emailPattern)){
                    Register.etEmail.setError("Invaild email!");
                }
                else if( Register.etPhoneNum.getText().toString().length() == 0 ) {
                    Register.etPhoneNum.setError("Phone is required!");
                }
                else if(!Register.cbIsAgree.isChecked()){
                    Register.cbIsAgree.setError("Please select agree");
                }


                if (  ( !Register.etUserName.getText().toString().equals("")) && ( !Register.etPassword.getText().toString().equals("")) && ( !Register.etCPassword.getText().toString().equals("")) && ( !Register.etEmail.getText().toString().equals("")) && ( !Register.etPhoneNum.getText().toString().equals("")) )
                {
                    if ( Register.etUserName.getText().toString().length() > 4 ){
                        if(isNetworkAvailable() == false){
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setMessage("Error in Network Connection")
                                    .setIcon(R.drawable.alert)
                                    .setCancelable(false)
                                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //  Action for 'NO' Button
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert = builder.create();
                            //Setting the title manually
                            alert.setTitle("Alert");
                            alert.show();
                            //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
                        }
                    }
                    else
                    {
                        //Toast.makeText(getApplicationContext(),"Username should be minimum 5 characters", Toast.LENGTH_SHORT).show();
                        Register.etUserName.setError( "Username should!" );
                    }
                }
                else
                {
                    //Toast.makeText(getApplicationContext(),"One or more fields are empty", Toast.LENGTH_SHORT).show();
                }

            }
        });

        Register.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Register.etPassword.setError(null);
                Register.etCPassword.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Register.etPassword.setError(null);
                Register.etCPassword.setError(null);
            }
        });

        Register.etCPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Register.etPassword.setError(null);
                Register.etCPassword.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Register.etPassword.setError(null);
                Register.etCPassword.setError(null);
            }
        });
    }



    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
