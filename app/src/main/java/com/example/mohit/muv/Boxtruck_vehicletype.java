
package com.example.mohit.muv;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.mohit.muv.LoginRegister.AppController;
import com.example.mohit.muv.Navigation_Drawer.First_Fragment;
import com.example.mohit.muv.Navigation_Drawer.First_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Second_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Third_Fragment;
import com.example.mohit.muv.R;
import com.example.mohit.muv.Fragments.Fragment_1;
import com.example.mohit.muv.Fragments.Fragment_2;
import com.example.mohit.muv.Fragments.Fragment_3;

import org.json.JSONArray;
import org.json.JSONObject;


public class Boxtruck_vehicletype extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    DrawerLayout mDrawer;
    NavigationView nvDrawer;
    ArrayList<HashMap<String,String >> addme;
    ProgressDialog progressDialog;
    String url;
    CategoryAdapter categoryAdapter;
    ListView vehicle_boxtruck;
    TextView vans,pickuptruck;
    LinearLayout pickup_lay,van_lay,box_lay;
    String pickaddress="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.boxtruck_vehicletype);
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        viewPager = (ViewPager) findViewById(R.id.viewpager);
//        setupViewPager(viewPager);
//        tabLayout = (TabLayout) findViewById(R.id.tabs);
//        tabLayout.setupWithViewPager(viewPager);
//        setupTabIcons();
        van_lay=(LinearLayout)findViewById(R.id.van_lay);
        pickup_lay=(LinearLayout)findViewById(R.id.pickup_lay);
        vans=(TextView)findViewById(R.id.vans);
        pickuptruck=(TextView)findViewById(R.id.pickuptruck);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        vehicle_boxtruck=(ListView)findViewById(R.id.vehicle_boxtruck);
        addme=new ArrayList<>();
        progressDialog=new ProgressDialog(Boxtruck_vehicletype.this);
        //  navigation_btn=(ImageView)findViewById(R.id.navigation_btn);
        setupDrawerContent(nvDrawer);
        toolbar.setNavigationIcon(R.drawable.navigation);
        toolbar.setTitle("MUV");
        toolbar.setTitleTextColor(Color.GRAY);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });
        vehicleCategory();
        vehicle_boxtruck.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle extras = getIntent().getExtras();
                try {
                    pickaddress = extras.getString("pickaddress");
                }catch(Exception e){}
                if((pickaddress == null) || (pickaddress.equals("")) ) {
                    Intent intent = new Intent(Boxtruck_vehicletype.this, Driver_detail.class);
                    intent.putExtra("hashme", addme);
                    intent.putExtra("position", position);
                    intent.putExtra("truckType", "boxtruck");
                    String book_btnvalue=extras.getString("book_btn");
                    intent.putExtra("book_btnv", book_btnvalue);
                    startActivity(intent);
                    overridePendingTransition(0,0);
                    finish();
                }
                else{
                    Intent intent = new Intent(Boxtruck_vehicletype.this, Driver_detail.class);
                    intent.putExtra("hashme", addme);
                    intent.putExtra("position", position);
                    String dropaddress = extras.getString("dropaddress");
                    String book_btnvalue=extras.getString("book_btn");
                    intent.putExtra("book_btnv", book_btnvalue);
                    int dollar = extras.getInt("dollar");
                    int passengerval = extras.getInt("passengerval");
                    double lat_pick = extras.getDouble("lat_pick");
                    double lon_pick = extras.getDouble("lon_pick");
                    double lat_drop = extras.getDouble("lat_drop");
                    double lon_drop = extras.getDouble("lon_drop");
                    double dist = extras.getDouble("dist");
                    String selectedImage=extras.getString("selectedImage");
                    intent.putExtra("pickaddress", pickaddress);
                    intent.putExtra("dropaddress", dropaddress);
                    intent.putExtra("dollar", dollar);
                    intent.putExtra("passengerval", passengerval);
                    intent.putExtra("lat_pick", lat_pick);
                    intent.putExtra("lon_pick", lon_pick);
                    intent.putExtra("lat_drop", lat_drop);
                    intent.putExtra("lon_drop", lon_drop);
                    intent.putExtra("dist", dist);
                    intent.putExtra("selectedImage", selectedImage);
                    intent.putExtra("truckType", "boxtruck");
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    finish();
                }

            }
        });
        van_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                try {
                    pickaddress = extras.getString("pickaddress");
                }catch(Exception e){}
                if((pickaddress == null) || (pickaddress.equals("")) ) {
                    Intent intent = new Intent(Boxtruck_vehicletype.this, Vans_vehicletype.class);
                    String book_btnvalue=extras.getString("book_btn");
                    intent.putExtra("book_btnv", book_btnvalue);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(0,0);
                }
                else{
                    Intent intent = new Intent(Boxtruck_vehicletype.this, Vans_vehicletype.class);
                    String dropaddress = extras.getString("dropaddress");
                    String book_btnvalue=extras.getString("book_btn");
                    intent.putExtra("book_btnv", book_btnvalue);
                    int dollar = extras.getInt("dollar");
                    int passengerval = extras.getInt("passengerval");
                    double lat_pick = extras.getDouble("lat_pick");
                    double lon_pick = extras.getDouble("lon_pick");
                    double lat_drop = extras.getDouble("lat_drop");
                    double lon_drop = extras.getDouble("lon_drop");
                    double dist = extras.getDouble("dist");
                    String selectedImage=extras.getString("selectedImage");
                    intent.putExtra("pickaddress", pickaddress);
                    intent.putExtra("dropaddress", dropaddress);
                    intent.putExtra("dollar", dollar);
                    intent.putExtra("passengerval", passengerval);
                    intent.putExtra("lat_pick", lat_pick);
                    intent.putExtra("lon_pick", lon_pick);
                    intent.putExtra("lat_drop", lat_drop);
                    intent.putExtra("lon_drop", lon_drop);
                    intent.putExtra("dist", dist);
                    intent.putExtra("selectedImage", selectedImage);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    finish();
                }



            }
        });
        pickup_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                try {
                    pickaddress = extras.getString("pickaddress");
                }catch(Exception e){}
                if((pickaddress == null) || (pickaddress.equals("")) ) {
                    Intent intent = new Intent(Boxtruck_vehicletype.this, Vehicle_type.class);
                    String book_btnvalue=extras.getString("book_btn");
                    intent.putExtra("book_btnv", book_btnvalue);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(0,0);
                }
                else{
                    Intent intent = new Intent(Boxtruck_vehicletype.this, Vehicle_type.class);
                    String dropaddress = extras.getString("dropaddress");
                    String book_btnvalue=extras.getString("book_btn");
                    intent.putExtra("book_btnv", book_btnvalue);
                    int dollar = extras.getInt("dollar");
                    int passengerval = extras.getInt("passengerval");
                    double lat_pick = extras.getDouble("lat_pick");
                    double lon_pick = extras.getDouble("lon_pick");
                    double lat_drop = extras.getDouble("lat_drop");
                    double lon_drop = extras.getDouble("lon_drop");
                    double dist = extras.getDouble("dist");
                    String selectedImage=extras.getString("selectedImage");
                    intent.putExtra("pickaddress", pickaddress);
                    intent.putExtra("dropaddress", dropaddress);
                    intent.putExtra("dollar", dollar);
                    intent.putExtra("passengerval", passengerval);
                    intent.putExtra("lat_pick", lat_pick);
                    intent.putExtra("lon_pick", lon_pick);
                    intent.putExtra("lat_drop", lat_drop);
                    intent.putExtra("lon_drop", lon_drop);
                    intent.putExtra("dist", dist);
                    intent.putExtra("selectedImage", selectedImage);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    finish();
                }



            }
        });
        vans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                try {
                    pickaddress = extras.getString("pickaddress");
                }catch(Exception e){}
                if((pickaddress == null) || (pickaddress.equals("")) ) {
                    Intent intent = new Intent(Boxtruck_vehicletype.this, Vans_vehicletype.class);
                    String book_btnvalue=extras.getString("book_btn");
                    intent.putExtra("book_btnv", book_btnvalue);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(0,0);
                }
                else{
                    Intent intent = new Intent(Boxtruck_vehicletype.this, Vans_vehicletype.class);
                    String dropaddress = extras.getString("dropaddress");
                    String book_btnvalue=extras.getString("book_btn");
                    intent.putExtra("book_btnv", book_btnvalue);
                    int dollar = extras.getInt("dollar");
                    int passengerval = extras.getInt("passengerval");
                    double lat_pick = extras.getDouble("lat_pick");
                    double lon_pick = extras.getDouble("lon_pick");
                    double lat_drop = extras.getDouble("lat_drop");
                    double lon_drop = extras.getDouble("lon_drop");
                    double dist = extras.getDouble("dist");
                    String selectedImage=extras.getString("selectedImage");
                    intent.putExtra("pickaddress", pickaddress);
                    intent.putExtra("dropaddress", dropaddress);
                    intent.putExtra("dollar", dollar);
                    intent.putExtra("passengerval", passengerval);
                    intent.putExtra("lat_pick", lat_pick);
                    intent.putExtra("lon_pick", lon_pick);
                    intent.putExtra("lat_drop", lat_drop);
                    intent.putExtra("lon_drop", lon_drop);
                    intent.putExtra("dist", dist);
                    intent.putExtra("selectedImage", selectedImage);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    finish();
                }



            }
        });
        pickuptruck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                try {
                    pickaddress = extras.getString("pickaddress");
                }catch(Exception e){}
                if((pickaddress == null) || (pickaddress.equals("")) ) {
                    Intent intent = new Intent(Boxtruck_vehicletype.this, Vehicle_type.class);
                    String book_btnvalue=extras.getString("book_btn");
                    intent.putExtra("book_btnv", book_btnvalue);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(0,0);
                }
                else{
                    Intent intent = new Intent(Boxtruck_vehicletype.this, Vehicle_type.class);
                    String dropaddress = extras.getString("dropaddress");
                    String book_btnvalue=extras.getString("book_btn");
                    intent.putExtra("book_btnv", book_btnvalue);
                    int dollar = extras.getInt("dollar");
                    int passengerval = extras.getInt("passengerval");
                    double lat_pick = extras.getDouble("lat_pick");
                    double lon_pick = extras.getDouble("lon_pick");
                    double lat_drop = extras.getDouble("lat_drop");
                    double lon_drop = extras.getDouble("lon_drop");
                    double dist = extras.getDouble("dist");
                    String selectedImage=extras.getString("selectedImage");
                    intent.putExtra("pickaddress", pickaddress);
                    intent.putExtra("dropaddress", dropaddress);
                    intent.putExtra("dollar", dollar);
                    intent.putExtra("passengerval", passengerval);
                    intent.putExtra("lat_pick", lat_pick);
                    intent.putExtra("lon_pick", lon_pick);
                    intent.putExtra("lat_drop", lat_drop);
                    intent.putExtra("lon_drop", lon_drop);
                    intent.putExtra("dist", dist);
                    intent.putExtra("selectedImage", selectedImage);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    finish();
                }



            }
        });
    }
    public void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override

                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }

                });
    }
    public void selectDrawerItem(MenuItem menuItem) {
//        Fragment fragment = null;
        // Class fragmentClass;
        switch(menuItem.getItemId()) {
            case R.id.nav_first_fragment:
                Intent intent=new Intent(Boxtruck_vehicletype.this,First_Fragment.class);
//                Toast.makeText(Boxtruck_vehicletype.this, "Go First fragment", Toast.LENGTH_SHORT).show();
                startActivity(intent);
                //finish();
                // fragmentClass = FirstFragment.class;
                break;
            case R.id.nav_second_fragment:
                Intent intent1=new Intent(Boxtruck_vehicletype.this,Second_Fragment.class);
                startActivity(intent1);
                //  fragmentClass = Second_Fragment.class;
                // finish();
                break;
//            case R.id.nav_third_fragment:
//
//                fragmentClass = ThirdFragment.class;
//
//                break;

            default:
                Intent intent11=new Intent(Boxtruck_vehicletype.this,First_Fragment.class);
//                Toast.makeText(Boxtruck_vehicletype.this, "Go First fragment", Toast.LENGTH_SHORT).show();
                startActivity(intent11);
                //finish();
                // fragmentClass = FirstFragment.class;
        }
        setTitle(menuItem.getTitle());
        mDrawer.closeDrawers();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }



        return super.onOptionsItemSelected(item);

    }

    public void vehicleCategory(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = "http://insonix.com/design.insonix.com/muv/get_driver_bytype.php?type=" + URLEncoder.encode("Box Truck");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    if(status.equalsIgnoreCase("ok")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject catlist = jsonArray.getJSONObject(i);
                            String id = catlist.getString("user_id");
                            String drivername = catlist.getString("drivername");
                            String driver_phone = catlist.getString("driver_phone");
                            String vehicle_type = catlist.getString("vehicle_type");
                            String vehicle_year = catlist.getString("vehicle_year");
                            String vehicle_make = catlist.getString("vehicle_make");
                            String vehicle_model = catlist.getString("vehicle_model");
                            String vehicle_number = catlist.getString("vehicle_number");
                            String rating = catlist.getString("rating");
                            Log.d("driverName", "driverName" + drivername);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("drivername", drivername);
                            hashMap.put("driver_phone", driver_phone);
                            hashMap.put("vehicle_type", vehicle_type);
                            hashMap.put("vehicle_year", vehicle_year);
                            hashMap.put("vehicle_make", vehicle_make);
                            hashMap.put("vehicle_model", vehicle_model);
                            hashMap.put("vehicle_number", vehicle_number);
                            hashMap.put("user_id", id);
                            hashMap.put("rating", rating);
                            addme.add(hashMap);
                        }
                        // addme = new ArrayList<>(addme);
                        vehicle_boxtruck.setAdapter(new CategoryAdapter(Boxtruck_vehicletype.this, addme));

                        //no_found.setVisibility(View.GONE);
                    }else{
                        new ShowMsg().createDialog(Boxtruck_vehicletype.this, "No Vehicle Available!");
                        // no_found.setVisibility(View.VISIBLE);
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class CategoryAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> addme;
        public CategoryAdapter(Activity activity,ArrayList<HashMap<String,String>>addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.vehicle_list,null);
//            ImageView cat_image=(ImageView)convertView.findViewById(R.id.cat_image);
            TextView drivrName=(TextView)convertView.findViewById(R.id.drivrName);
            //RelativeLayout rel_help=(RelativeLayout)convertView.findViewById(R.id.rel_help);
            drivrName.setText(addme.get(position).get("drivername"));
            RatingBar ratingVal=(RatingBar)convertView.findViewById(R.id.rtbProductRating);
            Log.d("Rating", "Rating" + addme.get(position).get("rating"));
            if(addme.get(position).get("rating").equals("") ){
            }else{
                ratingVal.setRating(Float.parseFloat(addme.get(position).get("rating")));
            }
//
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(getActivity(), Driver_detail.class);
//                    startActivity(intent);
//                }
//            });




            return convertView;
        }
    }



    //    /**
//     * Adding custom view to tab
//     */
//    private void setupTabIcons() {
//
//
//        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
//        tabOne.setText("Heavy Truck");
//        tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pickup_1, 0, 0, 0);
//        tabLayout.getTabAt(0).setCustomView(tabOne);
//
//
//        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
//        tabTwo.setText("Small Truck");
//        tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.van, 0, 0, 0);
//        tabLayout.getTabAt(1).setCustomView(tabTwo);
////        tabLayout.setSelectedTabIndicatorColor(R.color.colorAccent);
//
//        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
//        tabThree.setText("Mini Truck");
//        tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.box, 0, 0, 0);
//        tabLayout.getTabAt(2).setCustomView(tabThree);
//
//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//
//
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });
//    }
//
//    /**
//     * Adding fragments to ViewPager
//     * @param viewPager
//     */
//    private void setupViewPager(ViewPager viewPager) {
//        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        adapter.addFrag(new Fragment_1(Vehicle_type.this), "Heavy Truck");
//        adapter.addFrag(new Fragment_2(Vehicle_type.this), "Small Truck");
//        adapter.addFrag(new Fragment_3(Vehicle_type.this), "Mini Truck");
//        viewPager.setAdapter(adapter);
//    }
//
//    class ViewPagerAdapter extends FragmentPagerAdapter {
//        private final List<Fragment> mFragmentList = new ArrayList<>();
//        private final List<String> mFragmentTitleList = new ArrayList<>();
//
//        public ViewPagerAdapter(FragmentManager manager) {
//            super(manager);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            return mFragmentList.get(position);
//        }
//
//        @Override
//        public int getCount() {
//            return mFragmentList.size();
//        }
//
//        public void addFrag(Fragment fragment, String title) {
//            mFragmentList.add(fragment);
//            mFragmentTitleList.add(title);
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            return mFragmentTitleList.get(position);
//        }
//    }
    @Override
    public void onBackPressed() {
        //Include the code here
        Intent intent =new Intent(Boxtruck_vehicletype.this,Twoautocompletetetbox.class);
        startActivity(intent);
        finish();
    }
}
