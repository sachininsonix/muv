package com.example.mohit.muv;


import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.mohit.muv.Navigation_Drawer.First_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Navigation_drawer;
import com.example.mohit.muv.Navigation_Drawer.Second_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Third_Fragment;
import com.example.mohit.muv.constant.Iconstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mohit on 26-Feb-16.
 */
public class Booking_detail extends Activity implements Iconstant {
    Button book_now;
    DrawerLayout mDrawer;
    NavigationView nvDrawer;
    Toolbar toolbar;
    ArrayList<HashMap<String,String >> hashme;
    int position;
    TextView phonenum,bookname,location_name,distance,passenger_num,fair,vehicle_type,time,drop_location;
    SharedPreferences sharedpreferences;
    RatingBar ratingVal;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    String profile_url;
    ImageView goods_pic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_detail);

        phonenum=(TextView)findViewById(R.id.tvPhnNum);
        bookname=(TextView)findViewById(R.id.book_name);
        location_name=(TextView)findViewById(R.id.location_name);
        drop_location=(TextView)findViewById(R.id.drop_location);
        distance=(TextView)findViewById(R.id.distance);
        passenger_num=(TextView)findViewById(R.id.passenger_num);
        fair=(TextView)findViewById(R.id.fair);
        ratingVal=(RatingBar)findViewById(R.id.rtbProductRating);
        vehicle_type=(TextView)findViewById(R.id.vehicle_type);
        time=(TextView)findViewById(R.id.time);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        goods_pic=(ImageView)findViewById(R.id.sell_pic);
        imageLoader=ImageLoader.getInstance();

        imageLoader.init(ImageLoaderConfiguration.createDefault(Booking_detail.this));
        options = new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.blank).cacheOnDisc().cacheInMemory().build();

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        //  navigation_btn=(ImageView)findViewById(R.id.navigation_btn);
        setupDrawerContent(nvDrawer);
        toolbar.setNavigationIcon(R.drawable.navigation);
        toolbar.setTitle("MUV");
        toolbar.setTitleTextColor(Color.GRAY);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });

        hashme=new ArrayList<>();
        position=getIntent().getExtras().getInt("position");
        Intent intent=getIntent();
        hashme = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("hashme");
        //sharedpreferences.getString("galleryImages",galleryImages);
        Log.d("Name", "Name" + hashme.get(position).get("drivername"));
        Log.d("rating", "rating" + hashme.get(position).get("rating"));
        Log.d("Images", "Images" + sharedpreferences.getString(galleryImages, ""));
        phonenum.setText(hashme.get(position).get("phone"));
        bookname.setText(hashme.get(position).get("user_name"));
        location_name.setText(hashme.get(position).get("loc_name"));
        drop_location.setText(hashme.get(position).get("droplocation"));
        distance.setText(hashme.get(position).get("distance"));
        passenger_num.setText(hashme.get(position).get("no_of_pas"));
        fair.setText(hashme.get(position).get("fair"));
        time.setText(hashme.get(position).get("time"));

        profile_url=hashme.get(position).get("profile_file");
Log.d("profileURL","profile_url"+profile_url);
        if(profile_url.equals("")){

        }else{
            imageLoader.displayImage(profile_url, goods_pic, options);

        }
        try {
            if (hashme.get(position).get("rating").equals("")) {
            } else {
                ratingVal.setRating(Float.parseFloat(hashme.get(position).get("rating")));
            }
        }catch(Exception e){}

        phonenum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phonenum.getText().toString().trim()));//change the number
                startActivity(callIntent);
            }
        });
    }

    public void setupDrawerContent(NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(

                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override

                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        selectDrawerItem(menuItem);

                        return true;

                    }

                });

    }
    public void selectDrawerItem(MenuItem menuItem) {

        //Fragment fragment = null;
        //Class fragmentClass;

        switch(menuItem.getItemId()) {

            case R.id.nav_first_fragment:
                Intent intent=new Intent(Booking_detail.this,First_Fragment.class);
                startActivity(intent);
                // fragmentClass = FirstFragment.class;

                break;

            case R.id.nav_second_fragment:
                Intent intent1=new Intent(Booking_detail.this,Second_Fragment.class);
                startActivity(intent1);
                //  fragmentClass = Second_Fragment.class;
                break;
            case R.id.nav_third_fragment:
                // fragmentClass = Third_Fragment.class;
                break;
            default:
                Intent intent11=new Intent(Booking_detail.this,First_Fragment.class);
                startActivity(intent11);
                //  fragmentClass = Third_Fragment.class;
        }
        setTitle(menuItem.getTitle());
        mDrawer.closeDrawers();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                mDrawer.openDrawer(GravityCompat.START);

                return true;

        }



        return super.onOptionsItemSelected(item);

    }


}
