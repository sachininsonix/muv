package com.example.mohit.muv;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mohit.muv.LoginRegister.Login;
import com.example.mohit.muv.constant.Iconstant;
import com.facebook.login.LoginManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mohit on 23-Feb-16.
 */
public class Home extends FragmentActivity implements Iconstant {

    public static String email, username, uid;
    TextView tvEmail, tvUsername, tvUid;
    Button btnLogout,btnChooseMuv;


    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ListView listView;
    GoogleMap gmap;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    GPSTracker gpsTracker;
    double latitude;
    double longitude;
    //    GoogleMap
    ArrayList<HashMap<String, String>> addme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        preferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor=preferences.edit();

//        Intent rg = getIntent();
//
//        email = rg.getStringExtra("email");
//        username = rg.getStringExtra("username");
//        uid = rg.getStringExtra("uid");
//        tvEmail = (TextView)findViewById(R.id.email);
//        tvUsername = (TextView)findViewById(R.id.username);
//        tvUid = (TextView)findViewById(R.id.uid);
//        btnLogout = (Button) findViewById(R.id.btnLogout);
//        tvEmail.setText(preferences.getString(uEmail,""));
//        tvUsername.setText(preferences.getString(uUsername,""));
//        tvUid.setText(preferences.getString(uUid,""));

        btnChooseMuv = (Button)findViewById(R.id.btnChooseMuv);

        btnChooseMuv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Home.this, Vehicle_type.class);
                startActivity(i);
//                finish();
            }
        });

//        btnLogout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences preferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//                if(!preferences.getString(fUid,"").equals("")){
//                    LoginManager.getInstance().logOut();
//                }
//                editor.clear();
//                editor.commit();
//                Intent i = new Intent(Home.this, Login.class);
//                startActivity(i);
//                finish();
//            }
//        });
        //Toast.makeText(Home.this,"Email: "+email+" UserName: "+username+" Uid: "+uid,Toast.LENGTH_LONG).show();


        gpsTracker = new GPSTracker(Home.this);

        if (gpsTracker.canGetLocation) {

            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            Toast.makeText(Home.this, "Your Location is - \\nLat: " + latitude + "\\nLong: " + longitude, Toast.LENGTH_SHORT).show();
            // \n is for new line
//            Toast.makeText(Home.this, "Your Location is - \\nLat: " + latitude + "\\nLong: " + longitude, Toast.LENGTH_SHORT).show();
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
        } else {

            gpsTracker.showSettingsAlert();
        }

        try {
            // Loading map
            initilizeMap();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * function to load map. If map is not created it will create it for you
     */
    private void initilizeMap() {
        if (gmap == null) {
            gmap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

            // create marker.png
            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title(" ");

// adding marker.png
            // GREEN color icon
            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            // ROSE color icon
            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));


            gmap.addMarker(marker);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(12).build();

            gmap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


            // check if map is created successfully or not
            if (gmap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
