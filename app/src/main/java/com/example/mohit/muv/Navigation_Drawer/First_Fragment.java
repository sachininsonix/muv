package com.example.mohit.muv.Navigation_Drawer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.mohit.muv.Driver_detail;
import com.example.mohit.muv.LoginRegister.AppController;
import com.example.mohit.muv.LoginRegister.Login;
import com.example.mohit.muv.R;
import com.example.mohit.muv.ShowMsg;
import com.example.mohit.muv.constant.Iconstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by admin on 17-03-2016.
 */
public class First_Fragment extends Activity implements Iconstant {
    DrawerLayout mDrawer;
    NavigationView nvDrawer;
    Toolbar toolbar;
    ProgressDialog progressDialog;
    SharedPreferences sharedpreferences;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firstfragment);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        progressDialog=new ProgressDialog(First_Fragment.this);
        //  navigation_btn=(ImageView)findViewById(R.id.navigation_btn);
        setupDrawerContent(nvDrawer);
        toolbar.setNavigationIcon(R.drawable.navigation);
        toolbar.setTitle("MUV");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });

//        logoutProcess();
    }
    public void logoutProcess(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        //String url = "http://52.35.22.61/muv/login.php?email="+emailAddress.getText().toString()+"&password="+password.getText().toString()+"&device_id="+androidId;
       url = "http://insonix.com/design.insonix.com/muv/logout.php?id="+sharedpreferences.getString(uUid,"");


    Log.d("Url", "url" + url);
    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObject) {
            Log.d("res:", "resss" + jsonObject.toString());
            try{
                String status=jsonObject.getString("status");
                if(status.equals("ok")) {

//                        //{"mail":"Your have successfully entered","email":"amanwes@gmail.com","username":"amanas","phone":"1212323","uid":"2","status":"ok"}
//                        String message=response.getString("message");
//                        String email=response.getString("email");
//                        String username=response.getString("username");
//                        String uid=response.getString("uid");

                    Toast.makeText(First_Fragment.this, "Successfully Logout!", Toast.LENGTH_SHORT).show();

                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.clear();

//                        editor.putString(uUsername, "");
//
//                        editor.putString(uUid, "");
                    editor.commit();



                    Intent intent = new Intent(First_Fragment.this,Login.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(First_Fragment.this,"Some Error occured",Toast.LENGTH_LONG).show();
                }


            }catch(Exception e){
                e.printStackTrace();
                Log.d("eeee:","eee"+e);
            }
            progressDialog.dismiss();
        }

    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            Log.d("volleyError:","volleyError"+volleyError);
            progressDialog.dismiss();
        }


    });
    AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
}
    public void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override

                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }

                });
    }
    public void selectDrawerItem(MenuItem menuItem) {
        //Fragment fragment = null;
        //Class fragmentClass;

        switch(menuItem.getItemId()) {

            case R.id.nav_first_fragment:
                Intent intent=new Intent(First_Fragment.this,First_Fragment.class);
                startActivity(intent);
                // fragmentClass = FirstFragment.class;

                break;

            case R.id.nav_second_fragment:
                Intent intent1=new Intent(First_Fragment.this,Second_Fragment.class);
                startActivity(intent1);
                //  fragmentClass = Second_Fragment.class;

                break;

            case R.id.nav_third_fragment:
                Intent intent3=new Intent(First_Fragment.this,Third_Fragment.class);
                startActivity(intent3);
                // fragmentClass = Third_Fragment.class;

                break;

            default:
                Intent intent11=new Intent(First_Fragment.this,First_Fragment.class);
                startActivity(intent11);
                //  fragmentClass = Third_Fragment.class;

        }






        setTitle(menuItem.getTitle());

        mDrawer.closeDrawers();

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                mDrawer.openDrawer(GravityCompat.START);

                return true;

        }



        return super.onOptionsItemSelected(item);

    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
////        finish();
//    }
}
