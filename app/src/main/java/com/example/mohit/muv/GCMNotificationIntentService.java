package com.example.mohit.muv;

import android.app.AlertDialog;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.Iterator;
import java.util.Set;

public class GCMNotificationIntentService extends IntentService {
	// Sets an ID for the notification, so it can be updated
	//static final String MSG_KEY = "m";
	public static final int notifyID = 9001;
	NotificationCompat.Builder builder;
	SharedPreferences preferences ;
	SharedPreferences.Editor editor;
	String getclass,newString;


	public GCMNotificationIntentService() {
		super("GcmIntentService");

	}


	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		//extras.get("message");
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

		String messageType = gcm.getMessageType(intent);


		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				sendNotification("Send error: " + extras.toString(),""+extras.toString(),""+extras.toString(),"" +extras.toString(),
						"" +extras.toString(), "" +extras.toString(),""+extras.toString(),""+extras.toString(),""+extras.toString(),""+extras.toString(),"" +extras.toString(),"" +extras.toString(),""+extras.toString(),"" +extras.toString(),
						"" +extras.toString(),"" +extras.toString(),""+extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				sendNotification("Deleted messages on server: "
						+ extras.toString(),""+extras.toString(),""+extras.toString(),"" +extras.toString(),"" +extras.toString(), ""
						+ extras.toString(),""+extras.toString(),""+extras.toString(),""+extras.toString(),""+extras.toString(),""+extras.toString(),""+extras.toString(),"" +extras.toString(),"" +extras.toString(),"" +extras.toString(),"" +extras.toString(),"" +extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {
//				if(preferences.getBoolean("checked",true)) {
				sendNotification(" "
						+ extras.get("message"), "" + extras.get("user_name"), "" + extras.get("user_id"),
						"" +extras.get("driver_id"), "" +extras.get("user_phone"),"" + extras.get("distance"), "" +
								extras.get("booking_id"),
						"" + extras.get("fair"), "" + extras.get("loc_name"), "" + extras.get("droplocation"), "" + extras.get("no_of_pas"), ""
								+ extras.get("pick_up_lat") , "" + extras.get("pick_up_lon"), ""
								+ extras.get("drop_off_lat"), ""
								+ extras.get("drop_off_lon"), ""
								+ extras.get("profile_url"), ""+ extras.get("driver_phone"));
				Log.d("UName", "UName" + extras.get("name"));
//				}else{
//
//				}

				Set<String> sss= extras.keySet();
				Iterator it=sss.iterator();
				while (it.hasNext()) {
					Log.d("iteration", "it"+it.next());


				}
			}
		}
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendNotification(String message,String user_name,String user_id,String driver_id,
								  String user_phone,String distance,String booking_id,
								  String fair,String loc_name,String droplocation,String no_of_pas,
								  String pick_up_lat,String pick_up_lon,String drop_off_lat,String drop_off_lon,String profile_url,String driver_phone) {
		preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		editor = preferences.edit();
		PendingIntent resultPendingIntent;
		Intent resultIntent;
		String msg=message.trim();
//
// }else{
		if(msg.equals("Booking Request")){
			Log.d("user_name:","user_name"+ user_name);
			Log.d("user_id:","user_id"+ user_id);
			Log.d("driver_id:","driver_id"+ driver_id);
			Log.d("message:","message"+ message);
			resultIntent = new Intent(this, Booking_Notify.class);
			resultIntent.putExtra("message",msg);
			resultIntent.putExtra("user_name",user_name);
			resultIntent.putExtra("user_id",user_id);
			resultIntent.putExtra("driver_id",driver_id);
			resultIntent.putExtra("user_phone",user_phone);
			resultIntent.putExtra("distance",distance);
			resultIntent.putExtra("booking_id",booking_id);
			resultIntent.putExtra("fair",fair);
			resultIntent.putExtra("loc_name",loc_name);
			resultIntent.putExtra("droplocation",droplocation);
			resultIntent.putExtra("no_of_pas",no_of_pas);
			resultIntent.putExtra("pick_up_lat",pick_up_lat);
			resultIntent.putExtra("pick_up_lon",pick_up_lon);
			resultIntent.putExtra("drop_off_lat",drop_off_lat);
			resultIntent.putExtra("drop_off_lon",drop_off_lon);
			resultIntent.putExtra("profile_url",profile_url);
            resultIntent.putExtra("bookLater","");


			resultPendingIntent = PendingIntent.getActivity(this, 0,
					resultIntent, PendingIntent.FLAG_ONE_SHOT);
//
			NotificationCompat.Builder mNotifyBuilder;
			NotificationManager mNotificationManager;

			mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

			mNotifyBuilder = new NotificationCompat.Builder(this)
					.setSmallIcon(R.drawable.logo).setContentText(msg).setContentTitle("MUV");
			// Set pending intent
			mNotifyBuilder.setContentIntent(resultPendingIntent);

			// Set Vibrate, Sound and Light
			int defaults = 0;
			defaults = defaults | Notification.DEFAULT_LIGHTS;
			defaults = defaults | Notification.DEFAULT_VIBRATE;
			defaults = defaults | Notification.DEFAULT_SOUND;

			mNotifyBuilder.setDefaults(defaults);
			// Set the content for Notification
			// mNotifyBuilder.setContentText(msg);
			// Set autocancel
			mNotifyBuilder.setAutoCancel(true);
			// Post a notification
			mNotificationManager.notify(notifyID, mNotifyBuilder.build());
//
		}
		else if(msg.equals("Driver Accepted")) {
				resultIntent = new Intent(this, DriverMap.class);
				resultIntent.putExtra("message", msg);
				resultIntent.putExtra("user_name", user_name);
				resultIntent.putExtra("user_id", user_id);
				resultIntent.putExtra("driver_id", driver_id);
				resultIntent.putExtra("user_phone", user_phone);
				resultIntent.putExtra("distance", distance);
				resultIntent.putExtra("booking_id", booking_id);
				resultIntent.putExtra("fair", fair);
				resultIntent.putExtra("loc_name", loc_name);
				resultIntent.putExtra("droplocation", droplocation);
				resultIntent.putExtra("no_of_pas", no_of_pas);
				resultIntent.putExtra("pick_up_lat", pick_up_lat);
				resultIntent.putExtra("pick_up_lon", pick_up_lon);
				resultIntent.putExtra("drop_off_lat", drop_off_lat);
				resultIntent.putExtra("drop_off_lon", drop_off_lon);

				resultPendingIntent = PendingIntent.getActivity(this, 0,
						resultIntent, PendingIntent.FLAG_ONE_SHOT);
//
				NotificationCompat.Builder mNotifyBuilder;
				NotificationManager mNotificationManager;

				mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

				mNotifyBuilder = new NotificationCompat.Builder(this)
						.setSmallIcon(R.drawable.logo).setContentText(msg).setContentTitle("MUV");
				// Set pending intent
				mNotifyBuilder.setContentIntent(resultPendingIntent);

				// Set Vibrate, Sound and Light
				int defaults = 0;
				defaults = defaults | Notification.DEFAULT_LIGHTS;
				defaults = defaults | Notification.DEFAULT_VIBRATE;
				defaults = defaults | Notification.DEFAULT_SOUND;

				mNotifyBuilder.setDefaults(defaults);
				// Set the content for Notification
				// mNotifyBuilder.setContentText(msg);
				// Set autocancel
				mNotifyBuilder.setAutoCancel(true);
				// Post a notification
				mNotificationManager.notify(notifyID, mNotifyBuilder.build());
				// }
			}
		else if(msg.equals("You have a pending booking")) {
            resultIntent = new Intent(this, Booking_Notify.class);
            resultIntent.putExtra("message",msg);
            resultIntent.putExtra("user_name",user_name);
            resultIntent.putExtra("user_id",user_id);
            resultIntent.putExtra("driver_id",driver_id);
            resultIntent.putExtra("user_phone",driver_phone);
            resultIntent.putExtra("distance",distance);
            resultIntent.putExtra("booking_id",booking_id);
            resultIntent.putExtra("fair",fair);
            resultIntent.putExtra("loc_name",loc_name);
            resultIntent.putExtra("droplocation",droplocation);
            resultIntent.putExtra("no_of_pas",no_of_pas);
            resultIntent.putExtra("pick_up_lat",pick_up_lat);
            resultIntent.putExtra("pick_up_lon",pick_up_lon);
            resultIntent.putExtra("drop_off_lat",drop_off_lat);
            resultIntent.putExtra("drop_off_lon",drop_off_lon);
            resultIntent.putExtra("profile_url",profile_url);
            resultIntent.putExtra("bookLater","booklater");

            resultPendingIntent = PendingIntent.getActivity(this, 0,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);//
            NotificationCompat.Builder mNotifyBuilder;
            NotificationManager mNotificationManager;

            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.logo).setContentText(msg).setContentTitle("MUV");
            // Set pending intent
            mNotifyBuilder.setContentIntent(resultPendingIntent);
            // Set Vibrate, Sound and Light
            int defaults = 0;
            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
            defaults = defaults | Notification.DEFAULT_SOUND;
            mNotifyBuilder.setDefaults(defaults);
            // Set the content for Notification
            // mNotifyBuilder.setContentText(msg);
            // Set autocancel
            mNotifyBuilder.setAutoCancel(true);
            // Post a notification
            mNotificationManager.notify(notifyID, mNotifyBuilder.build());
//
		}
			else{

			Toast.makeText(getApplicationContext(), "Sorry! Driver rejected your Booking!", Toast.LENGTH_SHORT).show();
          resultIntent = new Intent(this, Twoautocompletetetbox.class);
				resultIntent.putExtra("message", msg);
				resultIntent.putExtra("user_name", user_name);
				resultIntent.putExtra("user_id", user_id);
				resultIntent.putExtra("driver_id", driver_id);
				resultIntent.putExtra("user_phone", user_phone);
				resultIntent.putExtra("distance", distance);
				resultIntent.putExtra("booking_id", booking_id);
				resultIntent.putExtra("fair", fair);
				resultIntent.putExtra("loc_name", loc_name);
				resultIntent.putExtra("droplocation", droplocation);
				resultIntent.putExtra("no_of_pas", no_of_pas);
				resultIntent.putExtra("pick_up_lat", pick_up_lat);
				resultIntent.putExtra("pick_up_lon", pick_up_lon);
				resultIntent.putExtra("drop_off_lat", drop_off_lat);
				resultIntent.putExtra("drop_off_lon", drop_off_lon);
				resultPendingIntent = PendingIntent.getActivity(this, 0,
						resultIntent, PendingIntent.FLAG_ONE_SHOT);
				NotificationCompat.Builder mNotifyBuilder;
				NotificationManager mNotificationManager;

				mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

				mNotifyBuilder = new NotificationCompat.Builder(this)
						.setSmallIcon(R.drawable.logo).setContentText(msg).setContentTitle("MUV");
				// Set pending intent
				mNotifyBuilder.setContentIntent(resultPendingIntent);

				// Set Vibrate, Sound and Light
				int defaults = 0;
				defaults = defaults | Notification.DEFAULT_LIGHTS;
				defaults = defaults | Notification.DEFAULT_VIBRATE;
				defaults = defaults | Notification.DEFAULT_SOUND;

				mNotifyBuilder.setDefaults(defaults);
				// Set the content for Notification
				// mNotifyBuilder.setContentText(msg);
				// Set autocancel
				mNotifyBuilder.setAutoCancel(true);
				// Post a notification
				mNotificationManager.notify(notifyID, mNotifyBuilder.build());
				// }
			}
//		resultIntent.putExtra("photo", image);
//		resultPendingIntent = PendingIntent.getActivity(this, 0,
//				resultIntent, PendingIntent.FLAG_ONE_SHOT);
////
//		NotificationCompat.Builder mNotifyBuilder;
//		NotificationManager mNotificationManager;
//		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//		mNotifyBuilder = new NotificationCompat.Builder(this)
//				.setSmallIcon(R.drawable.logo).setContentText(msg).setContentTitle("Smartcrop");
//		// Set pending intent
//		mNotifyBuilder.setContentIntent(resultPendingIntent);
//
//		// Set Vibrate, Sound and Light
//		int defaults = 0;
//		defaults = defaults | Notification.DEFAULT_LIGHTS;
//		defaults = defaults | Notification.DEFAULT_VIBRATE;
//		defaults = defaults | Notification.DEFAULT_SOUND;
//
//		mNotifyBuilder.setDefaults(defaults);
//		// Set the content for Notification
//		// mNotifyBuilder.setContentText(msg);
//		// Set autocancel
//		mNotifyBuilder.setAutoCancel(true);
//		// Post a notification
//		mNotificationManager.notify(notifyID, mNotifyBuilder.build());
	}
	private Object getFragmentManager() {
		// TODO Auto-generated method stub
		return null;
	}
}