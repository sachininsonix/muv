package com.example.mohit.muv.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.mohit.muv.Driver_history;
import com.example.mohit.muv.LoginRegister.AppController;
import com.example.mohit.muv.Navigation_Drawer.First_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Second_Fragment;
import com.example.mohit.muv.R;
import com.example.mohit.muv.ShowMsg;
import com.example.mohit.muv.constant.Iconstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by admin on 21-03-2016.
 */
public class Dialog_toggle_btn extends Activity implements CompoundButton.OnCheckedChangeListener,Iconstant{
    Button btn;
    Dialog dialog;
    DrawerLayout mDrawer;
    NavigationView nvDrawer;
    Toolbar toolbar;
    ProgressDialog progressDialog;
    String url;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_toggle_btn);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        progressDialog=new ProgressDialog(Dialog_toggle_btn.this);
        preferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor=preferences.edit();
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        //  navigation_btn=(ImageView)findViewById(R.id.navigation_btn);
        setupDrawerContent(nvDrawer);
        toolbar.setNavigationIcon(R.drawable.navigation);
        toolbar.setTitle("MUV");
        toolbar.setTitleTextColor(Color.GRAY);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);


            }
        });
//
        String available = preferences.getString(driverAvailable, "");
        Log.d("Available", "Available" + available);
        if(available.equals("1")){
            Intent intent = new Intent(Dialog_toggle_btn.this, Driver_history.class);
            startActivity(intent);
            finish();
        }
        else{
            dialog();
        }
    }
    void dialog(){

     dialog = new Dialog(Dialog_toggle_btn.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.driver_login_popup);
              dialog.show();
           SwitchCompat swt = (SwitchCompat)dialog.findViewById(R.id.Switch);
       // swt.setChecked (false);
        swt.setOnCheckedChangeListener(Dialog_toggle_btn.this);
//        swt.setChecked(true);
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.Switch:
                if(!isChecked){

//                    Toast.makeText(Dialog_toggle_btn.this, "Err Switch is off!!", Toast.LENGTH_SHORT).show();
                }else{
                    AvaialableDriver();

//                    Toast.makeText (Dialog_toggle_btn.this,"Yes Switch is on!!",Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }

    }
    public void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override

                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }

                });
    }
    public void selectDrawerItem(MenuItem menuItem) {
//        Fragment fragment = null;
        // Class fragmentClass;
        switch(menuItem.getItemId()) {
            case R.id.nav_first_fragment:
                Intent intent=new Intent(Dialog_toggle_btn.this,First_Fragment.class);

                startActivity(intent);
                //finish();
                // fragmentClass = FirstFragment.class;
                break;
            case R.id.nav_second_fragment:
                Intent intent1=new Intent(Dialog_toggle_btn.this,Second_Fragment.class);
                startActivity(intent1);
                //  fragmentClass = Second_Fragment.class;
                // finish();
                break;
//            case R.id.nav_third_fragment:
//
//                fragmentClass = ThirdFragment.class;
//
//                break;

            default:
                Intent intent11=new Intent(Dialog_toggle_btn.this,First_Fragment.class);

                startActivity(intent11);
                //finish();
                // fragmentClass = FirstFragment.class;
        }
        setTitle(menuItem.getTitle());
        mDrawer.closeDrawers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void AvaialableDriver(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = "http://insonix.com/design.insonix.com/muv/change_status.php?id="+preferences.getString(uUid,"") +"&available="+1;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    String message=jsonObject.getString("message");
                    if(status.equalsIgnoreCase("ok")) {
                        String available="1";
                        Log.d("available", "available" + available);
//                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        editor.putString(driverAvailable, available);
                        editor.commit();
                        Intent intent = new Intent(Dialog_toggle_btn.this, Driver_history.class);
                        startActivity(intent);
                        finish();
                    }else{
                        new ShowMsg().createDialog(Dialog_toggle_btn.this, message);
                        // no_found.setVisibility(View.VISIBLE);
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
}
