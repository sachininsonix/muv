package com.example.mohit.muv;
//https://guides.codepath.com/android/Fragment-Navigation-Drawer
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mohit.muv.Navigation_Drawer.First_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Second_Fragment;
import com.example.mohit.muv.constant.Iconstant;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimerTask;

import static android.view.View.GONE;

public class Twoautocompletetetbox extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,Iconstant {
    float disatnce;
    GPSTracker gpsTracker;
    double latitude,theta,dist,latitude1;
    CardView card_view;
    List<Address> address_start,address_drop;
    double longitude,dis,longitude1;
    View view1, view2, view3, view4;
    private AutoCompleteTextView autoComplete;
    static double inslat, inslang;
    private AutoCompleteTextView multiAutoComplete;
    static public LatLng latlng, latlng1;
    private ArrayAdapter<String> adapter1;
    static public Marker marker;
    ImageView navigation_btn;
    static public Marker marker1;
    double lat_pick,lon_pick,lat_drop,lon_drop;
    ImageView select_image;
    TextView passenger1, passenger2, passenger3, passenger4,tv_distance_time;
    String sp[];
    static TextView tvDistanceDuration,Fare_estimate,distanceinMiles,finalfare;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    GoogleApiClient mGoogleApiClient;
    static public GoogleMap googleMap;
    PlaceArrayAdapter adapter;
    static LinearLayout drop_layout, pick_layout, transparent_layout, image_layout,fare_lay;
    int w = 0, h = 0 , dollar;
    static RelativeLayout  spinner_view;

    static MarkerOptions markerOptions_cuurent;
    Dialog imageDialog;
    Location loc_start,loc_drop;
    // Spinner No_of_users;
    Button btn_now, btn_later;
    DrawerLayout mDrawer;
    NavigationView nvDrawer;
    Toolbar toolbar;
    ArrayList<HashMap<String,String >> addme;
    int count,passengerval=1;
    Bitmap[] thumbnails;
    boolean[] thumbnailsselection;
    String[] arrPath;
    String pickaddress,dropaddress,muvImage,distance_miles,EstimatedFare;
    Bitmap bitmap,bmp;
    Uri selectedImage;
    LatLngBounds.Builder builder;

//    ImageAdapter imageAdapter;


    //navigation_methode obj;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(28.70, -127.50), new LatLng(48.85, -55.19));
    Spinner spinner;
    TextView current_location,choose_location;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_twoautocompletetetbox);
        drop_layout = (LinearLayout) findViewById(R.id.drop_layout);
        //No_of_users = (Spinner) findViewById(R.id.No_of_users);
        // obj=new navigation_methode();
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        view1 = findViewById(R.id.view_1);
        view2 = findViewById(R.id.view_2);
        view3 = findViewById(R.id.view_3);
        view4 = findViewById(R.id.view_4);
        final Bundle bundle = new Bundle();


        current_location=(TextView)findViewById(R.id.current_location);

        choose_location=(TextView)findViewById(R.id.choose_location);
        distanceinMiles=(TextView)findViewById(R.id.distanceinMiles);
        passenger1 = (TextView) findViewById(R.id.passenger1);
        passenger2 = (TextView) findViewById(R.id.passenger2);
        passenger3 = (TextView) findViewById(R.id.passenger3);
        passenger4 = (TextView) findViewById(R.id.passenger4);
        card_view=(CardView)findViewById(R.id.card_view);
        select_image = (ImageView) findViewById(R.id.select_image);
        btn_later = (Button) findViewById(R.id.btn_later);
        btn_now = (Button) findViewById(R.id.btn_now);
        pick_layout = (LinearLayout) findViewById(R.id.pick_layout);
        transparent_layout = (LinearLayout) findViewById(R.id.transparent_layout);
        spinner_view = (RelativeLayout) findViewById(R.id.spinner_view);
        Fare_estimate = (TextView) findViewById(R.id.Fare_estimate);
        finalfare = (TextView) findViewById(R.id.finalfare);
        image_layout = (LinearLayout) findViewById(R.id.image_layout);
        fare_lay = (LinearLayout) findViewById(R.id.fare_lay);
        gpsTracker = new GPSTracker(Twoautocompletetetbox.this);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        //  navigation_btn=(ImageView)findViewById(R.id.navigation_btn);
        setupDrawerContent(nvDrawer);
        toolbar.setNavigationIcon(R.drawable.navigation);
        toolbar.setTitle("MUV");
        toolbar.setTitleTextColor(Color.GRAY);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);


            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(Twoautocompletetetbox.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        googleMap = mapFragment.getMap();
        // googleMap.setMyLocationEnabled(true);
        //  googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        if (gpsTracker.canGetLocation()) {

            if (gpsTracker.canGetLocation) {
                latitude = gpsTracker.getLatitude();
                longitude = gpsTracker.getLongitude();
//            Toast.makeText(Tw.this,"your location is enable"+latitude+"lon"+longitude,Toast.LENGTH_LONG).show();
            }
            LatLng latLng2 = new LatLng(latitude, longitude);

            markerOptions_cuurent = new MarkerOptions();

            markerOptions_cuurent.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
            Twoautocompletetetbox.googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng2));
            Twoautocompletetetbox.googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));
            googleMap.addMarker(markerOptions_cuurent.position(latLng2));
            // googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        } else {
//            gpsTracker.showSettingsAlert();

        }

//


        adapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);

        autoComplete = (AutoCompleteTextView) findViewById(R.id.edt_pick_up);
        multiAutoComplete = (AutoCompleteTextView) findViewById(R.id.edt_drop_down);
        // autoComplete.setHint("pick up location");
        autoComplete.setAdapter(adapter);
        multiAutoComplete.setAdapter(adapter);

        autoComplete.setThreshold(1);
        multiAutoComplete.setThreshold(1);
        btn_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((autoComplete.getText().length()>0)) {
                    if((autoComplete.getText().length()>0) && (multiAutoComplete.getText().length()> 0)) {
                        theta = lon_pick - lon_drop;
                        dist = Math.sin(deg2rad(lat_pick)) * Math.sin(deg2rad(lat_drop))
                                + Math.cos(deg2rad(lat_pick)) * Math.cos(deg2rad(lat_drop)) * Math.cos(deg2rad(theta));
                        dist = Math.acos(dist);
                        dist = rad2deg(dist);
                        dist = dist * 60 * 1.1515;
                        pickaddress = autoComplete.getText().toString().trim();
                        distance_miles = distanceinMiles.getText().toString().trim();
                        EstimatedFare = finalfare.getText().toString().trim();
                        dropaddress = multiAutoComplete.getText().toString().trim();
//                            distance_miles=String.format("%.2f", distance_miles);
                        dollar=Integer.parseInt(EstimatedFare);
                        Double Finaldistance=Double.parseDouble(distance_miles);
                        Log.d("Addres", "pickaddress" + pickaddress);
                        Log.d("Addres", "dropaddress" + dropaddress);
                        Log.d("lat_picklat_pick", "lat_pick" + lat_pick + "-lon:-" + lon_pick);
                        Log.d("lon_pick", "lon_pick" + lat_drop + "-lon:-" + lon_drop);
                        Log.d("dollar", "dollar" + dollar);
                        Log.d("distance_miles", "distance_miles" + Finaldistance);
                        Log.d("passengerval", "passengerval" + passengerval);
                        Log.d("selectedImage", "selectedImage" + selectedImage);

                        Intent intent = new Intent(Twoautocompletetetbox.this, Vehicle_type.class);
                        intent.putExtra("pickaddress", pickaddress);
                        intent.putExtra("dropaddress", dropaddress);
                        intent.putExtra("dollar", dollar);
                        intent.putExtra("dist", Finaldistance);
                        intent.putExtra("passengerval", passengerval);
                        intent.putExtra("lat_pick", lat_pick);
                        intent.putExtra("lon_pick", lon_pick);
                        intent.putExtra("lat_drop", lat_drop);
                        intent.putExtra("lon_drop", lon_drop);
                        intent.putExtra("book_btn", "book_now");
//                            intent.putExtra("bmp_Image", bmp);
                        try {
                            intent.putExtra("selectedImage", selectedImage.toString());
                        }catch (Exception e){
                            intent.putExtra("selectedImage", "");
                        }
                        startActivity(intent);
                        finish();
                    }
                    else  {
                        Intent intent = new Intent(Twoautocompletetetbox.this, Vehicle_type.class);
                        startActivity(intent);
                        finish();

                    }
//                        else{
//                            multiAutoComplete.setError("Drop Address is required!");
//                            multiAutoComplete.requestFocus();
//                        }
                }
                else{
//                        Intent intent = new Intent(Twoautocompletetetbox.this, Vehicle_type.class);
//                        startActivity(intent);
//                        finish();
                    autoComplete.setError("PickUp Address is required!");
                    autoComplete.requestFocus();
                }

            }
        });
        btn_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((autoComplete.getText().length()>0)) {
                    if((autoComplete.getText().length()>0) && (multiAutoComplete.getText().length()> 0)) {
                        theta = lon_pick - lon_drop;
                        dist = Math.sin(deg2rad(lat_pick)) * Math.sin(deg2rad(lat_drop))
                                + Math.cos(deg2rad(lat_pick)) * Math.cos(deg2rad(lat_drop)) * Math.cos(deg2rad(theta));
                        dist = Math.acos(dist);
                        dist = rad2deg(dist);
                        dist = dist * 60 * 1.1515;
                        pickaddress = autoComplete.getText().toString().trim();
                        distance_miles = distanceinMiles.getText().toString().trim();
                        EstimatedFare = finalfare.getText().toString().trim();
                        dropaddress = multiAutoComplete.getText().toString().trim();
//                            distance_miles=String.format("%.2f", distance_miles);
                        dollar=Integer.parseInt(EstimatedFare);
                        Double Finaldistance=Double.parseDouble(distance_miles);
                        Log.d("Addres", "pickaddress" + pickaddress);
                        Log.d("Addres", "dropaddress" + dropaddress);
                        Log.d("lat_picklat_pick", "lat_pick" + lat_pick + "-lon:-" + lon_pick);
                        Log.d("lon_pick", "lon_pick" + lat_drop + "-lon:-" + lon_drop);
                        Log.d("dollar", "dollar" + dollar);
                        Log.d("distance_miles", "distance_miles" + Finaldistance);
                        Log.d("passengerval", "passengerval" + passengerval);
                        Log.d("selectedImage", "selectedImage" + selectedImage);

                        Intent intent = new Intent(Twoautocompletetetbox.this, Vehicle_type.class);
                        intent.putExtra("pickaddress", pickaddress);
                        intent.putExtra("dropaddress", dropaddress);
                        intent.putExtra("dollar", dollar);
                        intent.putExtra("dist", Finaldistance);
                        intent.putExtra("passengerval", passengerval);
                        intent.putExtra("lat_pick", lat_pick);
                        intent.putExtra("lon_pick", lon_pick);
                        intent.putExtra("lat_drop", lat_drop);
                        intent.putExtra("lon_drop", lon_drop);
                        intent.putExtra("book_btn", "book_later");
//                            intent.putExtra("bmp_Image", bmp);
                        try {
                            intent.putExtra("selectedImage", selectedImage.toString());
                        }catch (Exception e){
                            intent.putExtra("selectedImage", "");
                        }
                        startActivity(intent);
                        finish();
                    }
                    else  {
                        Intent intent = new Intent(Twoautocompletetetbox.this, Vehicle_type.class);
                        startActivity(intent);
                        finish();

                    }
//                        else{
//                            multiAutoComplete.setError("Drop Address is required!");
//                            multiAutoComplete.requestFocus();
//                        }
                }
                else{
//                        Intent intent = new Intent(Twoautocompletetetbox.this, Vehicle_type.class);
//                        startActivity(intent);
//                        finish();
                    autoComplete.setError("PickUp Address is required!");
                    autoComplete.requestFocus();
                }
            }
        });

        try{
            autoComplete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    googleMap.clear();
                    googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    autoComplete.setError(null);
                    // googleMap.setMyLocationEnabled(true);
                    card_view.setVisibility(View.VISIBLE);
//                if(v.getId()==R.id.current_location){
                    current_location.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            googleMap.clear();
                            autoComplete.setCursorVisible(true);
                            hideSoftKeyboard();
//                            Toast.makeText(Twoautocompletetetbox.this,"current",Toast.LENGTH_LONG).show();
                            if (gpsTracker.canGetLocation()) {
                                if (gpsTracker.canGetLocation) {
                                    lat_pick = gpsTracker.getLatitude();
                                    lon_pick = gpsTracker.getLongitude();
                                }
                                latlng = new LatLng(lat_pick, lon_pick);
                                Geocoder gcd = new Geocoder(Twoautocompletetetbox.this, Locale.getDefault());
                                //   List<Address> addresses =
                                //         null;
                                try {
                                    address_start = gcd.getFromLocation(lat_pick, lon_pick,100);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                if (address_start.size() > 0 && address_start != null) {
                                    StringBuilder result = new StringBuilder();
                                    try {
                                        Log.d("pickAddress", "pickAddress" + address_start);
                                        if (address_start.get(0).getAddressLine(1) != null && !address_start.get(1).getAddressLine(0).isEmpty()
                                                && address_start.get(0).getFeatureName() != null && !address_start.get(0).getFeatureName().isEmpty()) {
                                            String address = address_start.get(0).getAddressLine(0);
                                            String address1 = address_start.get(0).getAddressLine(1);
                                            String knownName = address_start.get(0).getFeatureName();
                                            autoComplete.setText(address+"-"+address1+"-"+address_start.get(0).getLocality() + "-" + address_start.get(0).getAdminArea() + "-" + address_start.get(0).getCountryName());

                                        }else if (address_start.get(0).getAddressLine(0) != null && !address_start.get(0).getAddressLine(0).isEmpty()                                  ) {

                                            autoComplete.setText(address_start.get(0).getLocality() + "-" + address_start.get(0).getAdminArea() + "-" + address_start.get(0).getCountryName());

                                        }
                                        else {
                                            autoComplete.setText(address_start.get(0).getLocality() + "-" + address_start.get(0).getAdminArea() + "-" + address_start.get(0).getCountryName());

                                        }


                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                }
                                //
                                marker = googleMap.addMarker(new MarkerOptions()
                                        .title(address_start.get(0).getAdminArea())
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                                        .snippet("")
                                        .anchor(0.0f, 1.0f) // Anchors the marker.png on the bottom left
                                        .position(new LatLng(lat_pick, lon_pick)));

                                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
//

                            } else {
                                Intent intent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivityForResult(intent,2);
//                                gpsTracker.showSettingsAlert();
                            }
                            if (autoComplete == null) {
                                drop_layout.setVisibility(View.GONE);
                            } else {
                                drop_layout.setVisibility(View.VISIBLE);
                            }
                            card_view.setVisibility(GONE);
                        }

                    });
                    choose_location.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            autoComplete.setCursorVisible(true);
                            autoComplete.setText("");
                            autoComplete.setError(null);
//          Toast.makeText(Twoautocompletetetbox.this,"Choose",Toast.LENGTH_LONG).show();
                            autoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                                        long arg3) {
                                    hideSoftKeyboard();
                                    //  googleMap.clear();
//googleMap.setMyLocationEnabled(false);
                                    //              googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                                    googleMap.clear();
                                    Geocoder geocoder = new Geocoder(Twoautocompletetetbox.this);
                                    //   List<Address> addresses = null;
                                    String areaname = String.valueOf(arg0.getItemAtPosition(arg2));

                                    try {

                                        address_start = geocoder.getFromLocationName(areaname, 1);

                                        if (address_start.size() > 0) {
                                            lat_pick = address_start.get(0).getLatitude();
                                            lon_pick = address_start.get(0).getLongitude();
                                            inslat = lat_pick;
                                            inslang = lon_drop;
                                            String statename = address_start.get(0).getAdminArea();
                                            marker = googleMap.addMarker(new MarkerOptions()
                                                    .title(areaname)
                                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                                                    .snippet("SNIPPET")
                                                    .anchor(0.0f, 1.0f) // Anchors the marker.png on the bottom left
                                                    .position(new LatLng(lat_pick, lon_pick)));
                                            latlng = new LatLng(lat_pick, lon_pick);

                                            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
                                            // Zoom in the Google Map
                                            googleMap.animateCamera(CameraUpdateFactory.zoomTo(5));
                                            if (autoComplete == null) {
                                                drop_layout.setVisibility(View.GONE);
                                            } else {
                                                drop_layout.setVisibility(View.VISIBLE);
                                            }
//                                        Toast.makeText(getApplicationContext(), statename, Toast.LENGTH_LONG).show();

                                        }
                                    } catch (IOException e1) {
                                        e1.printStackTrace();
                                    }


                                }
                            });
                            card_view.setVisibility(GONE);

                        }
                    });

                }
                // else



            });


            multiAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                        long arg3) {
                    card_view.setVisibility(GONE);
                    hideSoftKeyboard();
                    Geocoder geocoder = new Geocoder(Twoautocompletetetbox.this);

                    String areaname = String.valueOf(arg0.getItemAtPosition(arg2));
                    try {
                        address_drop = geocoder.getFromLocationName(areaname, 1);

//
                        if (address_drop.size() > 0) {
                            lat_drop = address_drop.get(0).getLatitude();
                            lon_drop = address_drop.get(0).getLongitude();

//                        Toast.makeText(Twoautocompletetetbox.this,"dis"+dis,Toast.LENGTH_LONG).show();
                            String statename = address_drop.get(0).getAdminArea();
                            if(address_start.get(0).getCountryName().matches(address_drop.get(0).getCountryName())){
                                marker1 = googleMap.addMarker(new MarkerOptions()
                                        .title(areaname)
                                        .snippet("SNIPPET")
                                        .anchor(0.0f, 1.0f) // Anchors the marker.png on the bottom left
                                        .position(new LatLng(latitude, longitude)));

                                latlng1 = new LatLng(lat_drop, lon_drop);

                                ro1 obj = new ro1(Twoautocompletetetbox.this);

                                if (multiAutoComplete == null) {
                                    fare_lay.setVisibility(View.GONE);
                                } else {
                                    // Fare_estimate.setVisibility(View.VISIBLE);
                                    transparent_layout.setVisibility(View.VISIBLE);
                                    Animation slide = null;


                                    Animation slide1 = null;
                                    slide1  = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_scale);
                                    slide1.setStartOffset(2000);
                                    slide1.setDuration(4000);
                                    slide1.setFillAfter(true);


                                    fare_lay.setVisibility(View.VISIBLE);
                                    spinner_view.setVisibility(View.VISIBLE);
                                    image_layout.setVisibility(View.VISIBLE);
                                    Fare_estimate.startAnimation(slide1);
//
                                    slide1.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {
                                            pick_layout.setVisibility(GONE);
                                            drop_layout.setVisibility(GONE);
                                        }


                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            //  pick_layout.setVisibility(View.GONE);


                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {

                                        }
                                    });
                                }

//

                            }
                            else Toast.makeText(Twoautocompletetetbox.this,"Location is out of country",Toast.LENGTH_LONG).show();


                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }

            });}
        catch (Exception e){
            Toast.makeText(Twoautocompletetetbox.this,"Plz enter valid location",Toast.LENGTH_LONG).show();
        }
//
        spinner_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        passenger1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//

                view1.setVisibility(view1.VISIBLE);
                view2.setVisibility(GONE);
                view3.setVisibility(GONE);
                view4.setVisibility(GONE);
                passengerval=1;

            }
        });
        passenger2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view2.setVisibility(view2.VISIBLE);
                view1.setVisibility(GONE);
                view3.setVisibility(GONE);
                view4.setVisibility(GONE);
                passengerval=2;
            }
        });
        passenger3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view3.setVisibility(View.VISIBLE);
                view1.setVisibility(GONE);
                view2.setVisibility(GONE);
                view4.setVisibility(GONE);
                passengerval=3;

            }
        });
        passenger4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view4.setVisibility(View.VISIBLE);
                view1.setVisibility(GONE);
                view3.setVisibility(GONE);
                view2.setVisibility(GONE);
                passengerval=4;

            }
        });
        sp = new String[]{"1", "2", "3", "4"};
//

        image_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialogg();
                imageDialog.show();

            }
        });


    }
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public  double distance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c)/1000;

        return dist;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        Intent intent=new Intent(Twoautocompletetetbox.this,Twoautocompletetetbox.class);
//        startActivity(intent);
//    }

    void dialogg() {
        Display display = getWindowManager().getDefaultDisplay();
        w = display.getWidth();
        h = display.getHeight();
        int count;
        imageDialog = new Dialog(Twoautocompletetetbox.this);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = imageDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        imageDialog.setContentView(R.layout.options);
        LinearLayout approx_lay = (LinearLayout) imageDialog.findViewById(R.id.approx_lay);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w - 30, (h / 3) - 20);
        TextView options_camera = (TextView) imageDialog.findViewById(R.id.options_camera);
        TextView options_gallery = (TextView) imageDialog.findViewById(R.id.options_gallery);
        TextView options_cancel=(TextView)imageDialog.findViewById(R.id.options_cancel);
        approx_lay.setLayoutParams(params);
        options_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LinearLayout  twoautocomplete=(LinearLayout) findViewById(R.id.twoautocomplete);
                RelativeLayout  Gallerylayout=(RelativeLayout) findViewById(R.id.Gallerylayout);
                //  twoautocomplete.setVisibility(GONE);
                //Gallerylayout.setVisibility(View.VISIBLE);
                //  GalleryImage();
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
                Intent pick = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pick, 1);
            }
        });
        options_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent clik = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(clik, 0);
                imageDialog.dismiss();
            }
        });
        options_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog.dismiss();
            }
        });
    }

    class ViewHolder {
        ImageView imageview;
        CheckBox checkbox;
        int id;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK){
//
            imageDialog.dismiss();
            Toast.makeText(Twoautocompletetetbox.this, "Image has been succesfully uploaded!",Toast.LENGTH_SHORT).show();
        }
        if (requestCode == 1 && resultCode == Activity.RESULT_OK){
//            Uri selectedImage = imageReturnedIntent.getData();
//            select_image.setImageURI(selectedImage);
            imageDialog.dismiss();
            Toast.makeText(Twoautocompletetetbox.this, "Image has been succesfully uploaded!",Toast.LENGTH_SHORT).show();
        }
        if (requestCode == 2){
            gpsTracker = new GPSTracker(Twoautocompletetetbox.this);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (gpsTracker.canGetLocation()) {
                        if (gpsTracker.canGetLocation) {
                            lat_pick = gpsTracker.getLatitude();
                            lon_pick = gpsTracker.getLongitude();
                        }
                        latlng = new LatLng(lat_pick, lon_pick);
                        Geocoder gcd = new Geocoder(Twoautocompletetetbox.this, Locale.getDefault());
                        //   List<Address> addresses =
                        //         null;
                        try {
                            address_start = gcd.getFromLocation(lat_pick, lon_pick,100);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (address_start.size() > 0 && address_start != null) {
                            StringBuilder result = new StringBuilder();
                            try {
                                Log.d("pickAddress", "pickAddress" + address_start);
                                if (address_start.get(0).getAddressLine(1) != null && !address_start.get(1).getAddressLine(0).isEmpty()
                                        && address_start.get(0).getFeatureName() != null && !address_start.get(0).getFeatureName().isEmpty()) {
                                    String address = address_start.get(0).getAddressLine(0);
                                    String address1 = address_start.get(0).getAddressLine(1);
                                    String knownName = address_start.get(0).getFeatureName();
                                    autoComplete.setText(address+"-"+address1+"-"+address_start.get(0).getLocality() + "-" + address_start.get(0).getAdminArea() + "-" + address_start.get(0).getCountryName());

                                }else if (address_start.get(0).getAddressLine(0) != null && !address_start.get(0).getAddressLine(0).isEmpty()                                  ) {

                                    autoComplete.setText(address_start.get(0).getLocality() + "-" + address_start.get(0).getAdminArea() + "-" + address_start.get(0).getCountryName());

                                }
                                else {
                                    autoComplete.setText(address_start.get(0).getLocality() + "-" + address_start.get(0).getAdminArea() + "-" + address_start.get(0).getCountryName());

                                }


                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }
                        //
                        marker = googleMap.addMarker(new MarkerOptions()
                                .title(address_start.get(0).getAdminArea())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                                .snippet("")
                                .anchor(0.0f, 1.0f) // Anchors the marker.png on the bottom left
                                .position(new LatLng(lat_pick, lon_pick)));

                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
//                    LatLng latLng2 = new LatLng(latitude1, longitude1);
//
//                    markerOptions_cuurent = new MarkerOptions();
//
//                    markerOptions_cuurent.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
//                    Twoautocompletetetbox.googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng2));
                    Twoautocompletetetbox.googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));
//                    googleMap.addMarker(markerOptions_cuurent.position(latLng2));
                    // googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                } else {
//            gpsTracker.showSettingsAlert();
                    Intent intent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent,2);
                }

                }
            }, 500);


//


        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    selectedImage = imageReturnedIntent.getData();
                    Log.d("selectedImage", "selectedImage" + selectedImage);

//                    select_image.setImageURI(selectedImage);
//                    try {
//                        bitmap = new UserPicture(selectedImage, getContentResolver()).getBitmap();
//                    } catch (Exception e) {
//
//                    }
                }

                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    selectedImage = imageReturnedIntent.getData();

//                    select_image.setImageURI(selectedImage);
//                    Log.d("selectedImage","selectedImage"+selectedImage);
                }
                break;

        }
    }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //  getMenuInflater().inflate(R.menu.menu_twoautocompletetetbox, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(

                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override

                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        selectDrawerItem(menuItem);

                        return true;

                    }

                });

    }

    public void selectDrawerItem(MenuItem menuItem) {

//        Fragment fragment = null;
        // Class fragmentClass;

        switch(menuItem.getItemId()) {

            case R.id.nav_first_fragment:
                Intent intent=new Intent(Twoautocompletetetbox.this,First_Fragment.class);
                startActivity(intent);
//                finish();
                // fragmentClass = FirstFragment.class;

                break;

            case R.id.nav_second_fragment:
                Intent intent1=new Intent(Twoautocompletetetbox.this,Second_Fragment.class);
                startActivity(intent1);
                finish();
                //  fragmentClass = Second_Fragment.class;
//finish();
                break;

//            case R.id.nav_third_fragment:
//
//                fragmentClass = ThirdFragment.class;
//
//                break;

            default:
                Intent intent11=new Intent(Twoautocompletetetbox.this,First_Fragment.class);
                startActivity(intent11);
//                finish();
                //   finish();
                // fragmentClass = FirstFragment.class;

        }
        setTitle(menuItem.getTitle());

        mDrawer.closeDrawers();

    }

    //    public void selectDrawerItem(MenuItem menuItem) {
//
//        // Create a new fragment and specify the planet to show based on
//
//        // position
//
//        Fragment fragment = null;
//
//
//
//        Class fragmentClass;
//
//        switch(menuItem.getItemId()) {
//
//            case R.id.nav_first_fragment:
//
//                fragmentClass = First_Fragment.class;
//
//                break;
//
//            case R.id.nav_second_fragment:
//
//                fragmentClass = Second_Fragment.class;
//
//                break;
//
////            case R.id.nav_third_fragment:
////
////                fragmentClass = ThirdFragment.class;
////
////                break;
//
//            default:
//
//                fragmentClass = First_Fragment.class;
//
//        }
//
//
//
//        try {
//
//            fragment = (Fragment) fragmentClass.newInstance();
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//
//
//        // Insert the fragment by replacing any existing fragment
//
//  //      FragmentManager fragmentManager =getSupportFragmentManager();
//
////        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
//
//
//
//        // Highlight the selected item, update the title, and close the drawer
//
//        // Highlight the selected item has been done by NavigationView
//
//        // menuItem.setChecked(true);
//
//        setTitle(menuItem.getTitle());
//
//        mDrawer.closeDrawers();
//
//    }
    @Override
    public void onConnected(Bundle bundle) {
        adapter.setGoogleApiClient(mGoogleApiClient);
        //  Log.i(LOG_TAG, "Google Places API connected.");
    }

    @Override
    public void onConnectionSuspended(int i) {
        adapter.setGoogleApiClient(null);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}

class ro1 extends FragmentActivity implements LocationListener {

    // GoogleMap mGoogleMap;
    ArrayList<LatLng> mMarkerPoints;
    double mLatitude = 0;
    double mLongitude = 0,MinuteTotal;


    ro1(Twoautocompletetetbox c1) {

        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(c1);

        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available

            int requestCode = 20;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        } else { // Google Play Services are available

            // Initializing
            mMarkerPoints = new ArrayList<>();

//            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for Activity#requestPermissions for more details.
//                return TODO;
//            }
//            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for Activity#requestPermissions for more details.
//                return TODO;
//            }
            //  Twoautocompletetetbox.googleMap.setMyLocationEnabled(true);
            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) c1.getSystemService(LOCATION_SERVICE);
            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, true);
            Location location = locationManager.getLastKnownLocation(provider);
            if (location != null) {
                onLocationChanged(location);
            }
            locationManager.requestLocationUpdates(provider, 20000, 0, this);
            if (Twoautocompletetetbox.latlng != null && Twoautocompletetetbox.latlng1 != null) {
                mMarkerPoints.clear();

                Twoautocompletetetbox.googleMap.clear();
                // drawMarker(Twoautocompletetetbox.latlng1);
                drawMarker(Twoautocompletetetbox.latlng);
                drawMarker(Twoautocompletetetbox.latlng1);
                if (mMarkerPoints.size() >= 2) {
                    LatLng origin = Twoautocompletetetbox.latlng;
                    LatLng dest = Twoautocompletetetbox.latlng1;
                    // Getting URL to the Google Directions API
                    String url = getDirectionsUrl(origin, dest);
                    DownloadTask downloadTask = new DownloadTask();

                    // Start downloading json data from Google Directions API
                    downloadTask.execute(url);
                }
            }

        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            // Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    /**
     * A class to download data from Google Directions URL
     */
    private class DownloadTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Directions in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            String distance = "",duration = "", CurrentString,Currentdistance;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    if(j==0){    // Get distance from the list
                        distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get duration from the list
                        duration = (String)point.get("duration");
                        continue;
                    }
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }


                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(2);
                lineOptions.color(Color.RED);

            }
            CurrentString = duration;
            Currentdistance = distance;
            String[] spliteddistance = Currentdistance.split(" ");
            String split_onedistance = spliteddistance[0];
//         Double d = Double.parseDouble(s.replaceAll(",",""));
            Double split_ondistance = Double.parseDouble(split_onedistance.replaceAll(",",""));
            Double distanceMiles=(split_ondistance)/(1.609344);

            if(!CurrentString.contains("day") && !CurrentString.contains("days") && !CurrentString.contains("hour")
                    && !CurrentString.contains("hours")) {
                Log.d("Duration ", "Durationif" + "Distance:" + distance + ", Duration:" + duration);
                String[] splited = CurrentString.split(" ");
                String split_one = splited[0];
                String split_second = splited[1];
                String minuteS = "mins";
                String minuteOne = "minute";
                Double splitOne = Double.parseDouble(split_one);
                MinuteTotal=0;
                if (split_second.equalsIgnoreCase(minuteS)) {
                    Log.d("Splited String ", "minuteS String" + split_one + split_second );
                    MinuteTotal = (splitOne * 1);
                }
                if (split_second.equalsIgnoreCase(minuteOne)) {
                    Log.d("Splited String ", "minuteOne String" + split_one + split_second );
                    MinuteTotal = (splitOne * 1);
                }
                Log.d("MinutTotal", "MinutTotalif" + MinuteTotal);


            }
            else{
                MinuteTotal=0;
                Log.d("Duration ", "Durationelse" + "Distance:" + distance + ", Duration:" + duration);
                String[] splited = CurrentString.split(" ");
                String split_one = splited[0];

                String split_second = splited[1];
                String split_three = splited[2];
                String split_four = splited[3];
                String dayS = "days";
                String dayOne = "day";
                String hourS = "hours";
                String hourOne = "hour";
                String minuteS = "mins";
                String minuteOne = "minute";
                Double splitOne = Double.parseDouble(split_one);
                Double splitThree = Double.parseDouble(split_three);
                Log.d("Duration ", "Duration" + "Distance:" + distance + ", Duration:" + duration);
                if (split_second.equalsIgnoreCase(dayS)) {
                    Log.d("Splited String ", "Days String" + split_one + split_second + split_three);
                    try {
                        MinuteTotal = ((splitOne * 1440) + (splitThree * 60));
                    } catch (Exception e) {
                    }
                }
                if (split_second.equalsIgnoreCase(dayOne)) {
                    Log.d("Splited String ", "dayOne String" + split_one + split_second + split_three);
                    MinuteTotal = ((splitOne * 1440) + (splitThree * 60));
                }
                if (split_second.equalsIgnoreCase(hourS)) {
                    Log.d("Splited String ", "hourS String" + split_one + split_second + split_three);
                    MinuteTotal = ((splitOne * 60) + (splitThree * 1));
                }
                if (split_second.equalsIgnoreCase(hourOne)) {
                    Log.d("Splited String ", "hourOne String" + split_one + split_second + split_three);
                    MinuteTotal = ((splitOne * 60) + (splitThree * 1));
                }
                if (split_second.equalsIgnoreCase(minuteS)) {
                    Log.d("Splited String ", "minuteS String" + split_one + split_second + split_three);
                    MinuteTotal = (splitOne * 1);
                }
                if (split_second.equalsIgnoreCase(minuteOne)) {
                    Log.d("Splited String ", "minuteOne String" + split_one + split_second + split_three);
                    MinuteTotal = (splitOne * 1);
                }
                Log.d("MinutTotal", "MinutTotalelse" + MinuteTotal);

            }


            try {
                Log.d("MinutTotal", "MinutTotalFare" + MinuteTotal);
                Log.d("distanceMiles", "distanceMiles" + distanceMiles);
                int dollar = (int) (10 + (distanceMiles * 1.3) + (0.2 * MinuteTotal));
                Twoautocompletetetbox.Fare_estimate.setText("Fare Estimate $ " + dollar);
                Twoautocompletetetbox.distanceinMiles.setText(String.valueOf(distanceMiles));
                Twoautocompletetetbox.finalfare.setText(String.valueOf(dollar));



//                Animation slide1 = null;
//                slide1  = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_scale);
//                slide1.setDuration(5000);
//                slide1.setFillAfter(true);
//                //  slide1.setFillEnabled(true);
////                                slide1.setFillEnabled(true);
//
//                Twoautocompletetetbox.fare_lay.setVisibility(View.VISIBLE);
//                Twoautocompletetetbox.spinner_view.setVisibility(View.VISIBLE);
//                Twoautocompletetetbox.image_layout.setVisibility(View.VISIBLE);
//                Twoautocompletetetbox.Fare_estimate.startAnimation(slide1);
////                                spinner_view.startAnimation(slide1);
////                                image_layout.startAnimation(slide1);
//                slide1.setAnimationListener(new Animation.AnimationListener() {
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//                        Twoautocompletetetbox.pick_layout.setVisibility(GONE);
//                        Twoautocompletetetbox.drop_layout.setVisibility(GONE);
//                    }
//
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//                        //  pick_layout.setVisibility(View.GONE);
//
//
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//
//                    }
//                });
            }catch (Exception e){
                e.printStackTrace();
            }

            try{
                // Drawing polyline in the Google Map for the i-th route
                Twoautocompletetetbox.googleMap.addPolyline(lineOptions);
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
        /*
         * for (Marker marker : markers) {
         * builder.include(marker.getPosition()); }
         */
                for (LatLng point : points) {
                    builder.include(point);
                }

                LatLngBounds bounds = builder.build();
                int padding = 200; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
                        padding);
                Twoautocompletetetbox.googleMap.moveCamera(cu);
                Twoautocompletetetbox.googleMap.animateCamera(cu, 2000, null);
//    long timeDetail= TimeUnit.MILLISECONDS.toMinutes(duration));
//Log.d("Time duration","TimeDuration"+timeDetail);
            }
            catch (Exception e){
                e.printStackTrace();
//    Toast.makeText(getApplicationContext(),"valide location",Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void drawMarker(LatLng point) {
        mMarkerPoints.add(point);

        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();

        // Setting the position of the marker.png
        options.position(point);


        if (mMarkerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
        } else if (mMarkerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
        }

        // Add new marker.png to the Google Map Android API V2
        Twoautocompletetetbox.googleMap.addMarker(options);
    }

    @Override
    public void onLocationChanged(Location location) {
        // Draw the marker.png, if destination location is not set
        if (mMarkerPoints.size() < 2) {

            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
            LatLng point = new LatLng(mLatitude, mLongitude);

//            Twoautocompletetetbox.googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
//            Twoautocompletetetbox.googleMap.animateCamera(CameraUpdateFactory.zoomTo(7));

            drawMarker(point);
        }

    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }

}
class DirectionsJSONParser {

    /** Receives a JSONObject and returns a list of lists containing latitude and longitude */
    public List<List<HashMap<String,String>>> parse(JSONObject jObject){

        List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String,String>>>() ;
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;
        JSONObject jDistance = null;
        JSONObject jDuration = null;

        try {

            jRoutes = jObject.getJSONArray("routes");

            /** Traversing all routes */
            for(int i=0;i<jRoutes.length();i++){
                jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");

                List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();

                /** Traversing all legs */
                for(int j=0;j<jLegs.length();j++){

                    /** Getting distance from the json data */
                    jDistance = ((JSONObject) jLegs.get(j)).getJSONObject("distance");
                    HashMap<String, String> hmDistance = new HashMap<String, String>();
                    hmDistance.put("distance", jDistance.getString("text"));

                    /** Getting duration from the json data */
                    jDuration = ((JSONObject) jLegs.get(j)).getJSONObject("duration");
                    HashMap<String, String> hmDuration = new HashMap<String, String>();
                    hmDuration.put("duration", jDuration.getString("text"));

                    /** Adding distance object to the path */
                    path.add(hmDistance);

                    /** Adding duration object to the path */
                    path.add(hmDuration);

                    jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");

                    /** Traversing all steps */
                    for(int k=0;k<jSteps.length();k++){
                        String polyline = "";
                        polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);

                        /** Traversing all points */
                        for(int l=0;l<list.size();l++){
                            HashMap<String, String> hm = new HashMap<String, String>();
                            hm.put("lat", Double.toString(((LatLng)list.get(l)).latitude) );
                            hm.put("lng", Double.toString(((LatLng)list.get(l)).longitude) );
                            path.add(hm);
                        }
                    }
                }
                routes.add(path);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
        }
        return routes;
    }


//class DirectionsJSONParser {
//
//    /**
//     * Receives a JSONObject and returns a list of lists containing latitude and longitude
//     */
//    public List<List<HashMap<String, String>>> parse(JSONObject jObject) {
//
//        List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
//        JSONArray jRoutes = null;
//        JSONArray jLegs = null;
//        JSONArray jSteps = null;
//
//        try {
//
//            jRoutes = jObject.getJSONArray("routes");
//
//            /** Traversing all routes */
//            for (int i = 0; i < jRoutes.length(); i++) {
//                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
//                List path = new ArrayList<HashMap<String, String>>();
//
//                /** Traversing all legs */
//                for (int j = 0; j < jLegs.length(); j++) {
//                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");
//
//
//                    /** Traversing all steps */
//                    for (int k = 0; k < jSteps.length(); k++) {
//                        String polyline = "";
//                        polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
//                        List<LatLng> list = decodePoly(polyline);
//
//                        /** Traversing all points */
//                        for (int l = 0; l < list.size(); l++) {
//                            HashMap<String, String> hm = new HashMap<String, String>();
//                            hm.put("lat", Double.toString(((LatLng) list.get(l)).latitude));
//                            hm.put("lng", Double.toString(((LatLng) list.get(l)).longitude));
//                            path.add(hm);
//                        }
//                    }
//                    routes.add(path);
//                }
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//        }
//
//
//        return routes;
//    }


    /**
     * Method to decode polyline points
     * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     */
    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

}







