package com.example.mohit.muv.Navigation_Drawer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


import com.example.mohit.muv.R;

public class Navigation_drawer extends AppCompatActivity {
    private DrawerLayout mDrawer;
    NavigationView nvDrawer;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
       // toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        setupDrawerContent(nvDrawer);
//        nvDrawer.setNavigationItemSelectedListener(
//                new NavigationView.OnNavigationItemSelectedListener() {
//                    @Override
//                  public boolean onNavigationItemSelected(MenuItem menuItem) {
//                        // Handle menu item clicks here.
//                      //  mDrawer.openDrawer(GravityCompat.START);
//                        mDrawer.closeDrawers();  // CLOSE DRAWER
//                        return true;
//                    }
//                });
    }
    private void setupDrawerContent(NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(

                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override

                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        selectDrawerItem(menuItem);

                        return true;

                    }

                });

    }



    public void selectDrawerItem(MenuItem menuItem) {

        // Create a new fragment and specify the planet to show based on

        // position

        Fragment fragment = null;



        Class fragmentClass;

        switch(menuItem.getItemId()) {

            case R.id.nav_first_fragment:
Intent intent=new Intent(Navigation_drawer.this,First_Fragment.class);
                startActivity(intent);
               // fragmentClass = FirstFragment.class;

                break;

            case R.id.nav_second_fragment:
                Intent intent1=new Intent(Navigation_drawer.this,Second_Fragment.class);
                startActivity(intent1);
              //  fragmentClass = Second_Fragment.class;

                break;

//            case R.id.nav_third_fragment:
//
//                fragmentClass = ThirdFragment.class;
//
//                break;

            default:
                Intent intent11=new Intent(Navigation_drawer.this,First_Fragment.class);
                startActivity(intent11);
               // fragmentClass = FirstFragment.class;

        }



        try {

          //  fragment = (Fragment) fragmentClass.newInstance();

        } catch (Exception e) {

            e.printStackTrace();

        }



        // Insert the fragment by replacing any existing fragment

     //   FragmentManager fragmentManager = getSupportFragmentManager();

       // fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();



        // Highlight the selected item, update the title, and close the drawer

        // Highlight the selected item has been done by NavigationView

        // menuItem.setChecked(true);

        setTitle(menuItem.getTitle());

        mDrawer.closeDrawers();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                mDrawer.openDrawer(GravityCompat.START);

                return true;

        }



        return super.onOptionsItemSelected(item);
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
    }
}
