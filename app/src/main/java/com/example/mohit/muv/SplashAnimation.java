package com.example.mohit.muv;

import android.*;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mohit.muv.Fragments.Dialog_toggle_btn;
import com.example.mohit.muv.LoginRegister.Login;
import com.example.mohit.muv.constant.Iconstant;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

/**
 * Created by insonix on 14/3/16.
 */
public class SplashAnimation extends Activity implements Iconstant {
    //ImageView tvMuvText;
    MediaPlayer mediaPlayer,mp;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ImageView siple_logo,text_muv,logo_light,logo_grill;
    private String regid = null;

    protected String SENDER_ID="125774750686";
    private GoogleCloudMessaging gcm=null;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    GPSTracker gpsTracker;
    LocationAddress locationAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animation_screen);
        final NetworkInfo activeNetwork;
        ConnectivityManager cm =
                (ConnectivityManager) SplashAnimation.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetwork = cm.getActiveNetworkInfo();
        final boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        preferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor=preferences.edit();
        siple_logo=(ImageView)findViewById(R.id.siple_logo);
        text_muv=(ImageView)findViewById(R.id.text_muv);
        logo_light=(ImageView)findViewById(R.id.logo_light);
        logo_grill=(ImageView)findViewById(R.id.logo_grill);
//        mediaPlayer = MediaPlayer.create(SplashAnimation.this,R.raw.car_start);
        gpsTracker=new GPSTracker(SplashAnimation.this);
        double latitude = gpsTracker.getLatitude();
        double longitude = gpsTracker.getLongitude();
        Log.d("latitude:","latitude"+latitude);
        Log.d("longitude:", "longitude" + longitude);
        if(preferences.getString(uUid, "").equals("")) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (ContextCompat.checkSelfPermission(SplashAnimation.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(SplashAnimation.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(SplashAnimation.this, android.Manifest.permission.VIBRATE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(SplashAnimation.this, android.Manifest.permission.INTERNET)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(SplashAnimation.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(SplashAnimation.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(SplashAnimation.this, android.Manifest.permission.ACCESS_WIFI_STATE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(SplashAnimation.this, android.Manifest.permission.ACCESS_NETWORK_STATE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(SplashAnimation.this, android.Manifest.permission.MANAGE_DOCUMENTS)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(SplashAnimation.this, android.Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(SplashAnimation.this, android.Manifest.permission.WAKE_LOCK)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SplashAnimation.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                    android.Manifest.permission.VIBRATE, android.Manifest.permission.INTERNET,
                                    android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION,
                                    android.Manifest.permission.ACCESS_WIFI_STATE, android.Manifest.permission.ACCESS_NETWORK_STATE,
                                    android.Manifest.permission.MANAGE_DOCUMENTS, android.Manifest.permission.CALL_PHONE, android.Manifest.permission.WAKE_LOCK},
                            1);

                }
            }
        }else{

        }

        if(checkPlayServices()){
            registerInBackground();
        }else{

        }
  final Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.myanimation);
       final Animation left_right = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.left_right);
        final Animation right_left = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.right_left);


        Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);
       // siple_logo.startAnimation(slide_down);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
//        RelativeLayout l=(RelativeLayout) findViewById(R.id.lin_lay);
//        l.clearAnimation();
//        l.startAnimation(anim);
        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
//        ImageView iv = (ImageView) findViewById(R.id.tyre);
//        iv.setVisibility(View.VISIBLE);
//        iv.clearAnimation();
//        iv.startAnimation(anim);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds
//                handler.postDelayed(this);
                try {
                    mediaPlayer.prepare();
                }
                catch(Exception e){
                }
                text_muv.setVisibility(View.VISIBLE);
                text_muv.startAnimation(right_left);
            }
        }, 1000);
//        text_muv.setVisibility(View.VISIBLE);
//        text_muv.startAnimation(right_left);
        final Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds
//                handler.postDelayed(this);
                logo_light.setVisibility(View.VISIBLE);
                logo_light.startAnimation(right_left);
            }
        }, 4000);
        final Handler grillhandler = new Handler();
        grillhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds
//                handler.postDelayed(this);
                logo_grill.setVisibility(View.VISIBLE);
                logo_grill.startAnimation(right_left);
            }
        }, 4000);
      Handler handler2=new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                    mp=MediaPlayer.create(SplashAnimation.this,R.raw.car_start);
                    try {
                        if (mp.isPlaying()) {
                            mp.stop();
                            mp.release();
                            mp=MediaPlayer.create(SplashAnimation.this,R.raw.car_start);
                        }
                        mp.start();
                    } catch (Exception e) {
                    }
            }
        }, 5000);
        if (gpsTracker.canGetLocation()) {
        } else {
            gpsTracker.showSettingsAlert();



        }
//if(siple_logo.getVisibility()==View.VISIBLE && iv.getVisibility()==View.VISIBLE){
//    text_muv.setVisibility(View.VISIBLE);
//    text_muv.startAnimation(left_right);
//}
        if (isConnected == true) {


            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(8000);

                                if (preferences.getString(uUid, "").equals("")) {

                                    Intent i = new Intent(SplashAnimation.this, Login.class);
                                    startActivity(i);

                                    //Remove activity
                                    finish();
                                } else if (preferences.getString(uType, "").equals("1")) {
                                    Intent intent = new Intent(SplashAnimation.this, Dialog_toggle_btn.class);
                                    startActivity(intent);
                                    finish();
                                } else {

                                    Intent i = new Intent(SplashAnimation.this, Twoautocompletetetbox.class);
                                    startActivity(i);

                                    //Remove activity
                                    finish();
                                }


//                            } else {
//                                if (preferences.getString(uUid, "").equals("")) {
//
//                                    Intent i = new Intent(SplashAnimation.this, Login.class);
//                                    startActivity(i);
//
//                                    //Remove activity
//                                    finish();
//                                } else if (preferences.getString(uType, "").equals("1")) {
//                                    Intent intent = new Intent(SplashAnimation.this, Dialog_toggle_btn.class);
//                                    startActivity(intent);
//                                    finish();
//                                } else {
//
//                                    Intent i = new Intent(SplashAnimation.this, Twoautocompletetetbox.class);
//                                    startActivity(i);
//
//                                    //Remove activity
//                                    finish();
//                                }
//                            }
//                        } else {
//                            if (preferences.getString(uUid, "").equals("")) {
//
//                                Intent i = new Intent(SplashAnimation.this, Login.class);
//                                startActivity(i);
//
//                                //Remove activity
//                                finish();
//                            } else if (preferences.getString(uType, "").equals("1")) {
//                                Intent intent = new Intent(SplashAnimation.this, Dialog_toggle_btn.class);
//                                startActivity(intent);
//                                finish();
//                            } else {
//
//                                Intent i = new Intent(SplashAnimation.this, Twoautocompletetetbox.class);
//                                startActivity(i);
//
//                                //Remove activity
//                                finish();
//                            }
//                        }
                        if (mp.isPlaying()) {
                            mp.stop();
                            mp.release();
                            mp=MediaPlayer.create(SplashAnimation.this,R.raw.car_start);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();



        }

        else {
            Toast.makeText(SplashAnimation.this, " No Internet connection ", Toast.LENGTH_LONG).show();
        }

    }
    @Override protected void onResume()
    {
        super.onResume();
        checkPlayServices();
    }
    private boolean checkPlayServices() {
        int resultcode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (resultcode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultcode)) {
                GooglePlayServicesUtil.getErrorDialog(resultcode, SplashAnimation.this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {

            }
            return false;
        }
        return true;
    }
    public void  registerInBackground(){
        new AsyncTask<Void,Void,String>(){

            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging
                                .getInstance(getApplicationContext());
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Registration ID :" + regid;
                    Log.i("Registration id", regid);

                    editor.putString("reg_id", regid);
                    editor.commit();


                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (!TextUtils.isEmpty(regid)) {
                    // storeRegIdinSharedPref(getActivity(), regId, emailID);
						/*
						 * Toast.makeText( con,
						 * "Registered with GCM Server successfully.\n\n" + msg,
						 * Toast.LENGTH_SHORT).show();
						 */
                } else {
						/*
						 * Toast.makeText( con,
						 * "Reg ID Creation Failed.\n\nEither you haven't enabled Internet or GCM server is busy right now. Make sure you enabled Internet and try registering again after some time."
						 * + msg, Toast.LENGTH_LONG).show();
						 */
                }
            }
        }.execute(null, null, null);
    }
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
//                    String languageToLoad = "en";
//                Locale locale = new Locale(languageToLoad);
//                Locale.setDefault(locale);
//                Configuration config = new Configuration();
//                config.locale = locale;
//                getBaseContext().getResources().updateConfiguration(config,
//                        getBaseContext().getResources().getDisplayMetrics());
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
//        mediaPlayer.release();
        mp.release();

    }
}
