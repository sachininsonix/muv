package com.example.mohit.muv.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.mohit.muv.Driver_detail;
import com.example.mohit.muv.Home;
import com.example.mohit.muv.LoginRegister.AppController;
import com.example.mohit.muv.R;
import com.example.mohit.muv.ShowMsg;
import com.example.mohit.muv.VehicleAdaptor;
import com.example.mohit.muv.Vehicle_type;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by karundhawan on 28-01-2016.
 */
public class Fragment_2 extends Fragment {

    static  ListView taboneList;
    Context f2;
    ArrayList<HashMap<String,String >> addme;
    ProgressDialog progressDialog;
    String url;
   CategoryAdapter categoryAdapter;
    public Fragment_2(Vehicle_type vehicle_type) {
        f2 = vehicle_type;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_2,container,false);
        taboneList = (ListView)view.findViewById(R.id.taboneList);
        VehicleAdaptor vehicleAdaptor = new VehicleAdaptor((Vehicle_type) f2);
        taboneList.setAdapter(vehicleAdaptor);
        addme=new ArrayList<>();
        progressDialog=new ProgressDialog(getActivity());
//        taboneList.setAdapter(vehicleAdaptor);
        taboneList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(f2, Driver_detail.class);
                intent.putExtra("hashme", addme);
                intent.putExtra("position",position);
                startActivity(intent);

            }
        });

        vehicleCategory();

        return view;
    }

    public void vehicleCategory(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = "http://insonix.com/design.insonix.com/muv/get_driver_bytype.php?type=" + URLEncoder.encode("Small truck");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    if(status.equalsIgnoreCase("ok")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject catlist = jsonArray.getJSONObject(i);
                            String id = catlist.getString("user_id");
                            String driverName = catlist.getString("drivername");
                            String driver_phone = catlist.getString("driver_phone");
                            String vehicle_type = catlist.getString("vehicle_type");
                            String vehicle_year = catlist.getString("vehicle_year");
                            String vehicle_make = catlist.getString("vehicle_make");
                            String vehicle_model = catlist.getString("vehicle_model");
                            String vehicle_number = catlist.getString("vehicle_number");

                            Log.d("driverName", "driverName" + driverName);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("drivername", driverName);
                            hashMap.put("driver_phone", driver_phone);
                            hashMap.put("vehicle_type", vehicle_type);
                            hashMap.put("vehicle_year", vehicle_year);
                            hashMap.put("vehicle_make", vehicle_make);
                            hashMap.put("vehicle_model", vehicle_model);
                            hashMap.put("vehicle_number", vehicle_number);
                            hashMap.put("user_id", id);
                            addme.add(hashMap);
                        }
                        // addme = new ArrayList<>(addme);
                        categoryAdapter = new CategoryAdapter(getActivity(), addme);
                        taboneList.setAdapter(categoryAdapter);
                        //no_found.setVisibility(View.GONE);
                    }else{
                        new ShowMsg().createDialog(getActivity(), "No record found");
                        // no_found.setVisibility(View.VISIBLE);
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class CategoryAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> addme;
        public CategoryAdapter(Activity activity,ArrayList<HashMap<String,String>>addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.vehicle_list,null);
//            ImageView cat_image=(ImageView)convertView.findViewById(R.id.cat_image);
            TextView drivrName=(TextView)convertView.findViewById(R.id.drivrName);
            //RelativeLayout rel_help=(RelativeLayout)convertView.findViewById(R.id.rel_help);
            drivrName.setText(addme.get(position).get("drivername"));
//
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(getActivity(), Driver_detail.class);
//                    startActivity(intent);
//                }
//            });




            return convertView;
        }
    }
}
