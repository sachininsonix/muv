package com.example.mohit.muv;


import android.app.Activity;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.mohit.muv.LoginRegister.AppController;
import com.example.mohit.muv.Navigation_Drawer.First_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Navigation_drawer;
import com.example.mohit.muv.Navigation_Drawer.Second_Fragment;
import com.example.mohit.muv.Navigation_Drawer.Third_Fragment;
import com.example.mohit.muv.constant.Iconstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mohit on 26-Feb-16.
 */
public class Booking_Notify extends Activity implements Iconstant {
    Button book_now,accept,reject;
    DrawerLayout mDrawer;
    NavigationView nvDrawer;
    Toolbar toolbar;
    ArrayList<HashMap<String,String >> hashme;
    int position;
    TextView phonenum,bookname,location_name,distance,passenger_num,fair,vehicle_type,time,drop_location;
    SharedPreferences sharedpreferences;
    GPSTracker gpsTracker;
    LocationAddress locationAddress;
    ProgressDialog progressDialog;
    String url,user_id,booking_id,pick_up_lat,pick_up_lon,drop_off_lat,drop_off_lon,loc_name,message,totDistanc,
    profile_url,totdistance,numberOnly,aplhabetonly,book_later;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    double latitude,longitude;
    ImageView goods_pic;
    String request_status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_notify);
        imageLoader=ImageLoader.getInstance();

            imageLoader.init(ImageLoaderConfiguration.createDefault(Booking_Notify.this));
            options = new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.blank).cacheOnDisc().cacheInMemory().build();

        phonenum=(TextView)findViewById(R.id.tvPhnNum);
        bookname=(TextView)findViewById(R.id.book_name);
        location_name=(TextView)findViewById(R.id.location_name);
        drop_location=(TextView)findViewById(R.id.drop_location);
        distance=(TextView)findViewById(R.id.distance);
        passenger_num=(TextView)findViewById(R.id.passenger_num);
        fair=(TextView)findViewById(R.id.fair);
        vehicle_type=(TextView)findViewById(R.id.vehicle_type);
        accept=(Button)findViewById(R.id.accept);
        goods_pic=(ImageView)findViewById(R.id.goods_pic);
        reject=(Button)findViewById(R.id.reject);
        time=(TextView)findViewById(R.id.time);
        progressDialog=new ProgressDialog(Booking_Notify.this);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        gpsTracker=new GPSTracker(Booking_Notify.this);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        //  navigation_btn=(ImageView)findViewById(R.id.navigation_btn);
        setupDrawerContent(nvDrawer);
        toolbar.setNavigationIcon(R.drawable.navigation);
        toolbar.setTitle("MUV");
        toolbar.setTitleTextColor(Color.GRAY);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);


            }
        });

        Log.d("latitude:", "latitude" + latitude);
        Log.d("longitude:", "longitude" + longitude);
        if (gpsTracker.canGetLocation()) {
//            double latitude = gpsTracker.getLatitude();
//            double longitude = gpsTracker.getLongitude();
//            String languageToLoad = "en";
//            Locale locale = new Locale(languageToLoad);
//            Locale.setDefault(locale);
//            Configuration config = new Configuration();
//            config.locale = locale;
//            getBaseContext().getResources().updateConfiguration(config,
//                    getBaseContext().getResources().getDisplayMetrics());
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation(latitude, longitude,
                    getApplicationContext(), new GeocoderHandler());

            // Log.d("locationAddress:", "locationAddress" + locationAddress);
        } else {
            gpsTracker.showSettingsAlert();
        }

        Intent intent=getIntent();
        Bundle extras = getIntent().getExtras();

//        hashme = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("hashme");
        //sharedpreferences.getString("galleryImages",galleryImages);
//        Log.d("Name", "Name" + hashme.get(position).get("drivername"));
//        Log.d("Images","Images"+sharedpreferences.getString(galleryImages, ""));
        try {
            message = extras.getString("message");
            user_id = extras.getString("user_id");
            booking_id = extras.getString("booking_id");
            loc_name = extras.getString("loc_name");
            pick_up_lat = extras.getString("pick_up_lat");
            pick_up_lon = extras.getString("pick_up_lon");
            drop_off_lat = extras.getString("drop_off_lat");
            drop_off_lon = extras.getString("drop_off_lon");
            totDistanc = extras.getString("distance");
            phonenum.setText(extras.getString("user_phone"));
            bookname.setText(extras.getString("user_name"));
            location_name.setText(extras.getString("loc_name"));
            drop_location.setText(extras.getString("droplocation"));
            distance.setText(extras.getString("distance"));
            passenger_num.setText(extras.getString("no_of_pas"));
            fair.setText(extras.getString("fair"));
            profile_url=extras.getString("profile_url");
            book_later=extras.getString("bookLater");
Log.d("BookLater","BookLater"+book_later);
            if(profile_url.equals("")){

            }else{
                imageLoader.displayImage(profile_url, goods_pic, options);

            }
            phonenum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + phonenum.getText().toString().trim()));//change the number
                    startActivity(callIntent);
                }
            });
//        time.setText(extras.getString("time"));
            if(book_later.equals("booklater")){
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        request_status="1";
                        UserAccept();

                    }
                });
                reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        request_status="0";
                        UserAccept();
                    }
                });
            }
            else {
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        request_status="1";
                        DriverAccept();

                    }
                });
                reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        request_status="0";
                        DriverAccept();
                    }
                });
            }
        }catch (Exception e){

        }

    }

    public void DriverAccept(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        url = "http://insonix.com/design.insonix.com/muv/response_booking.php?driver_id="+
                sharedpreferences.getString(uUid,"")+"&user_id="+user_id+"&request_status="+request_status+"&booking_id="+booking_id+
                "&lat="+latitude+"&lon="+longitude;
        Log.d("Url", "url" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    String message1=jsonObject.getString("Message");
                    if(status.equalsIgnoreCase("true")) {
                        if(request_status.equals("1")) {
                            Intent intent = new Intent(Booking_Notify.this, DriverMap.class);
                            intent.putExtra("loc_name", loc_name);
                            intent.putExtra("pick_up_lat", pick_up_lat);
                            intent.putExtra("pick_up_lon", pick_up_lon);
                            intent.putExtra("drop_off_lat", drop_off_lat);
                            intent.putExtra("drop_off_lon", drop_off_lon);
                            intent.putExtra("booking_id", booking_id);
                            intent.putExtra("distance", totDistanc);
                            intent.putExtra("user_id", user_id);
                            intent.putExtra("message", message);
                            startActivity(intent);
                            finish();
                        }else{
                            Intent intent= new Intent(Booking_Notify.this,Driver_history.class);
                            startActivity(intent);
                            finish();
                        }
//                        JSONArray jsonArray = jsonObject.getJSONArray("bookings");
//                        new ShowMsg().createDialog(Booking_Notify.this, message);
                    }else{
//                        new ShowMsg().createDialog(Booking_Notify.this, message);
                        // no_found.setVisibility(View.VISIBLE);
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    public void UserAccept(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
         url = "http://insonix.com/design.insonix.com/muv/pending_booking.php?logged_id="+
                sharedpreferences.getString(uUid,"")+"&booking_status="+request_status+"&booking_id="+booking_id;
        Log.d("Url", "url" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    String message=jsonObject.getString("Message");
                    if(status.equalsIgnoreCase("true")) {
                        if(message.equals("Accepted")){
                        Intent intent= new Intent(Booking_Notify.this,Book_thanks.class);
                            intent.putExtra("bookbtn","book_now");
                            startActivity(intent);
                            finish();}
                        else{
                            new ShowMsg().createDialogSignUp(Booking_Notify.this, "You have rejected that Booking!");

                        }
//                        JSONArray jsonArray = jsonObject.getJSONArray("bookings");
//                        new ShowMsg().createDialog(Booking_Notify.this, message);
                    }else{
//                        new ShowMsg().createDialog(Booking_Notify.this, message);
                        // no_found.setVisibility(View.VISIBLE);
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    public void setupDrawerContent(NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(

                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override

                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        selectDrawerItem(menuItem);

                        return true;

                    }

                });

    }
    public void selectDrawerItem(MenuItem menuItem) {

        //Fragment fragment = null;
        //Class fragmentClass;

        switch(menuItem.getItemId()) {

            case R.id.nav_first_fragment:
                Intent intent=new Intent(Booking_Notify.this,First_Fragment.class);
                startActivity(intent);
                // fragmentClass = FirstFragment.class;

                break;

            case R.id.nav_second_fragment:
                Intent intent1=new Intent(Booking_Notify.this,Second_Fragment.class);
                startActivity(intent1);
                //  fragmentClass = Second_Fragment.class;

                break;

            case R.id.nav_third_fragment:

                // fragmentClass = Third_Fragment.class;

                break;

            default:
                Intent intent11=new Intent(Booking_Notify.this,First_Fragment.class);
                startActivity(intent11);
                //  fragmentClass = Third_Fragment.class;

        }

        setTitle(menuItem.getTitle());

        mDrawer.closeDrawers();

    }
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
//                    String languageToLoad = "en";
//                Locale locale = new Locale(languageToLoad);
//                Locale.setDefault(locale);
//                Configuration config = new Configuration();
//                config.locale = locale;
//                getBaseContext().getResources().updateConfiguration(config,
//                        getBaseContext().getResources().getDisplayMetrics());
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }



        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            if(book_later.equals("booklater")){
                Intent intent = new Intent(Booking_Notify.this, Twoautocompletetetbox.class);
                startActivity(intent);
                finish();
            }else {
                Intent intent = new Intent(Booking_Notify.this, Driver_history.class);
                startActivity(intent);
                finish();
            }
        }catch (Exception e){

        }
    }
}
